package EvidencijaStudenata;

import EvidencijaStudenata.Controller.AddToTableViewController;
import EvidencijaStudenata.Controller.InitializeCall;
import EvidencijaStudenata.Model.AddToTableModel;
import EvidencijaStudenata.Model.StudyGroupModel;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddToTableOpener {
    // KLASA KOJA OTVARA ZAJEDNICKI VIEW SVIM TABELAMA ZA DODAVANJE U BAZU 

    public void openAndInitAddToTableView(String[] labelsText, String viewName, String caption, String insertSQL, InitializeCall object, String[] selectedData,
                                          boolean hasForeignKey, ObservableList<AddToTableModel> list){
        try {
            Parent root;
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(viewName));
            root = loader.load();
            AddToTableViewController attvc = loader.getController();
            attvc.initLabelsAndTextFields(labelsText, labelsText.length, caption, insertSQL, object, selectedData, hasForeignKey, list);

            Stage stage = new Stage();
            stage.setTitle(caption);
            stage.setScene(new Scene(root, 1000, 500));
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(AddToTableOpener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    }

