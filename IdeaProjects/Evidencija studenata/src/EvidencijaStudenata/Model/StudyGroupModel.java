package EvidencijaStudenata.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudyGroupModel {
    SimpleStringProperty idStudijskaGrupa = new SimpleStringProperty();
    SimpleStringProperty nazivStudijskaGrupa = new SimpleStringProperty();

    public StudyGroupModel(String idStudijskaGrupa, String nazivStudijskaGrupa){
        this.idStudijskaGrupa = new SimpleStringProperty(idStudijskaGrupa);
        this.nazivStudijskaGrupa = new SimpleStringProperty(nazivStudijskaGrupa);
    }

    public String getIdStudijskaGrupa() {
        return idStudijskaGrupa.get();
    }

    public String getNazivStudijskaGrupa() {
        return nazivStudijskaGrupa.get();
    }

//-------------------------------------------------------------------------------------------------------
//------------------------------------------------GETTERI------------------------------------------------
//-------------------------------------------------------------------------------------------------------
public static ObservableList<StudyGroupModel> listStudyGroup() {
    ObservableList<StudyGroupModel> list = FXCollections.observableArrayList();
    DatabaseClass db = new DatabaseClass();
    ResultSet rs = db.select("SELECT * FROM studijska_grupa");

    try {
        while (rs.next()) {
            list.add(new StudyGroupModel(rs.getString("idStudijskaGrupa"), rs.getString("nazivStudijskaGrupa")));
        }
    } catch (SQLException e) {
        System.out.println("Greska prilikom iteracije " + e.getMessage());
    }

    return list;
}

    public static ObservableList<StudyGroupModel> listStudyGroupForSpecificCurr(String predmet_id) {
        ObservableList<StudyGroupModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM studijska_grupa WHERE idStudijskaGrupa IN (SELECT studijska_grupa_id " +
                "FROM predmet_studijska_grupa WHERE predmet_id = '" + predmet_id + "')");
        try {
            while (rs.next()) {
                list.add(new StudyGroupModel(rs.getString("idStudijskaGrupa"), rs.getString("nazivStudijskaGrupa")));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static ObservableList<String> listStudyGroupIDs() {
        ObservableList<String> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT idStudijskaGrupa FROM studijska_grupa");

        try {
            while (rs.next()) {
                list.add(rs.getString("idStudijskaGrupa"));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public String[] getStudyGroupRowByID() {
        String resultArray[] = new String[2];
        resultArray[0] = idStudijskaGrupa.getValue();

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM studijska_grupa WHERE idStudijskaGrupa = '" + idStudijskaGrupa.getValue() + "'");

        try {
            while (rs.next()) {
                resultArray[1] = (rs.getString("nazivStudijskaGrupa"));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
        return resultArray;
    }
    //-------------------------------------------------------------------------------------------------------
//------------------------------------------------SETTERI------------------------------------------------
//-------------------------------------------------------------------------------------------------------
    public void deleteStudyGroup(){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement delete = db.exec("DELETE FROM studijska_grupa WHERE idStudijskaGrupa=?");
            delete.setString(1, idStudijskaGrupa.getValue());
            delete.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(StudyGroupModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
