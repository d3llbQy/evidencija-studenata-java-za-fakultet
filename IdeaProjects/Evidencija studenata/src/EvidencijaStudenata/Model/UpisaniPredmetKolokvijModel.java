package EvidencijaStudenata.Model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.String.valueOf;

public class UpisaniPredmetKolokvijModel {
SimpleIntegerProperty id_kolokvij_upisani_predmet;
SimpleStringProperty brojBodovaKolokvij;
SimpleIntegerProperty kolokvij_id;
SimpleIntegerProperty upisani_predmet_id;
SimpleStringProperty idBrIndexStudent;
SimpleStringProperty nameAndSurnameStudent;
SimpleStringProperty nazivKolokvij;

public UpisaniPredmetKolokvijModel(int id_kolokvij_upisani_predmet, String brojBodovaKolokvij, int kolokvij_id, int upisani_predmet_id, String idBrIndexStudent,
                                   String nameAndSurnameStudent, String nazivKolokvij){
    this.id_kolokvij_upisani_predmet = new SimpleIntegerProperty(id_kolokvij_upisani_predmet);
    this.brojBodovaKolokvij = new SimpleStringProperty(brojBodovaKolokvij);
    this.kolokvij_id = new SimpleIntegerProperty(kolokvij_id);
    this.upisani_predmet_id = new SimpleIntegerProperty(upisani_predmet_id);
    this.idBrIndexStudent = new SimpleStringProperty(idBrIndexStudent);
    this.nameAndSurnameStudent = new SimpleStringProperty(nameAndSurnameStudent);
    this.nazivKolokvij = new SimpleStringProperty(nazivKolokvij);
}

    public int getId_kolokvij_upisani_predmet() {
        return id_kolokvij_upisani_predmet.get();
    }

    public String getBrojBodovaKolokvij() {
        return brojBodovaKolokvij.get();
    }

    public int getKolokvij_id() {
        return kolokvij_id.get();
    }

    public int getUpisani_predmet_id() {
        return upisani_predmet_id.get();
    }

    public String getIdBrIndexStudent() {
        return idBrIndexStudent.get();
    }

    public String getNameAndSurnameStudent() {
        return nameAndSurnameStudent.get();
    }

    public String getNazivKolokvij() {
        return nazivKolokvij.get();
    }

    public static ObservableList<UpisaniPredmetKolokvijModel> upisaniPredmetKolokvijList(String studijska_grupa_id, int kolokvij_id){
        ObservableList<UpisaniPredmetKolokvijModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT stud.idBrIndexSTudent, stud.imeStudent, stud.prezimeStudent, kup.id_kolokvij_upisani_predmet, "+
                "kup.brojBodovaKolokvij, kup.upisani_predmet_id FROM student stud, kolokvij_upisani_predmet kup " +
                "WHERE stud.idBrIndexStudent IN " +
        "(SELECT student_id FROM upisani_predmet WHERE idUpisaniPredmet = kup.upisani_predmet_id) AND " +
        "kup.kolokvij_id = " + kolokvij_id + " AND stud.studijska_grupa_id = '" + studijska_grupa_id + "'");

        try {
            while (rs.next()) {
                list.add(new UpisaniPredmetKolokvijModel((rs.getInt("kup.id_kolokvij_upisani_predmet")), (rs.getString("kup.brojBodovaKolokvij")),
                        kolokvij_id, (rs.getInt("kup.upisani_predmet_id")), (rs.getString("stud.idBrIndexStudent")),
                        (rs.getString("stud.imeStudent") + " " + (rs.getString("stud.prezimeStudent"))), null));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static ObservableList<UpisaniPredmetKolokvijModel> upisaniPredmetKolokvijListByStudentID(String idBrIndexStudent, String predmet_id){
        ObservableList<UpisaniPredmetKolokvijModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT kol.nazivKolokvij, kup.id_kolokvij_upisani_predmet, kup.brojBodovaKolokvij, kup.upisani_predmet_id, kup.kolokvij_id " +
        "FROM kolokvij kol, kolokvij_upisani_predmet kup WHERE kup.upisani_predmet_id = " +
        "(SELECT idUpisaniPredmet FROM upisani_predmet WHERE student_id = '" + idBrIndexStudent + "' AND predmet_id = '" + predmet_id + "') " +
                "AND kol.idKolokvij = kup.kolokvij_id");
        try {
            while (rs.next()) {
                list.add(new UpisaniPredmetKolokvijModel((rs.getInt("kup.id_kolokvij_upisani_predmet")), (rs.getString("kup.brojBodovaKolokvij")),
                        (rs.getInt("kup.kolokvij_id")), (rs.getInt("kup.upisani_predmet_id")), idBrIndexStudent,
                        null, (rs.getString("kol.nazivKolokvij"))));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public String[] getUPKRowByID(){
        String resultArray[] = new String[2];
        resultArray[0] = valueOf(id_kolokvij_upisani_predmet.getValue());

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM kolokvij_upisani_predmet WHERE id_kolokvij_upisani_predmet = " + valueOf(id_kolokvij_upisani_predmet.getValue()));

        try {
            while (rs.next()) {
                resultArray[1] = (rs.getString("brojBodovaKolokvij"));

            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
        return resultArray;
    }

}
