package EvidencijaStudenata.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserModel {
    SimpleStringProperty idUser = new SimpleStringProperty();
    SimpleStringProperty imeUser = new SimpleStringProperty();
    SimpleStringProperty prezimeUser = new SimpleStringProperty();
    SimpleStringProperty username = new SimpleStringProperty();
    SimpleStringProperty rolaUser = new SimpleStringProperty();
    SimpleStringProperty imePrezimeUser = new SimpleStringProperty();

    public UserModel(String idUser, String imeUser, String prezimeUser, String usernameUser, String rolaUser) {
        this.idUser = new SimpleStringProperty(idUser);
        this.imeUser = new SimpleStringProperty(imeUser);
        this.prezimeUser = new SimpleStringProperty(prezimeUser);
        this.username = new SimpleStringProperty(usernameUser);
        this.rolaUser = new SimpleStringProperty(rolaUser);
        this.imePrezimeUser = new SimpleStringProperty(imeUser + prezimeUser);
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------GETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------

    public String getIdUser() {
        return idUser.get();
    }

    public String getImeUser() {
        return imeUser.get();
    }

    public String getPrezimeUser() {
        return prezimeUser.get();
    }

    public String getUsername() {
        return username.get();
    }

    public String getRolaUser() {
        return rolaUser.get();
    }

    public static ObservableList<UserModel> listUser() {
        ObservableList<UserModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM user");

        try {
            while (rs.next()) {
                list.add(new UserModel(rs.getString("idUser"), rs.getString("imeUser"), rs.getString("prezimeUser"), rs.getString("username"),
                        rs.getString("rolaUser")));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static ObservableList<UserModel> listProfessor() {
        ObservableList<UserModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM user WHERE rolaUser='profesor'");

        try {
            while (rs.next()) {
                list.add(new UserModel(rs.getString("idUser"), rs.getString("imeUser"), rs.getString("prezimeUser"), rs.getString("username"),
                        rs.getString("rolaUser")));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static ObservableList<UserModel> listProfessorOnCurr(String predmetId) {
        ObservableList<UserModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM user " +
                "WHERE idUser = (SELECT profesor_id FROM predmet_profesor WHERE predmet_id = '" + predmetId + "')");

        try {
            while (rs.next()) {
                list.add(new UserModel(rs.getString("idUser"), rs.getString("imeUser"), rs.getString("prezimeUser"), rs.getString("username"),
                        rs.getString("rolaUser")));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static String getUserPassword(String userName) {
        // HANDLEOVATI AKO IMA VISE USERNAMEOVA
        String userId ="";
        String userPassword="";

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT idUser FROM user WHERE userName='" + userName + "'");

        try {
            while (rs.next()) {
                userId = rs.getString("idUser");
            }
        } catch (SQLException e) {
                // HANDLEOVATI!!!
        }

        rs = db.select(("SELECT password FROM user WHERE idUser = '" + userId + "'"));

        try {
            while (rs.next()) {
                userPassword = rs.getString("password");
            }
        } catch (SQLException e) {
            // HANDLEOVATI!!!
        }
        return userPassword;
    }

    public static String getUserId(String userName){
        String userId = "";

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT idUser FROM user WHERE userName='" + userName + "'");

        try {
            while (rs.next()) {
                userId = rs.getString("idUser");
            }
        } catch (SQLException e) {
            // HANDLEOVATI!!!
        }
        return userId;
    }

    public static String getUserRole(String userId){
        String userRole = "";

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT rolaUser FROM user WHERE idUser='" + userId + "'");

        try {
            while (rs.next()) {
                userRole = rs.getString("rolaUser");
            }
        } catch (SQLException e) {
            // HANDLEOVATI!!!
        }
        return userRole;

    }

    public static String getUserName(String userId){
        String userNameAndSurname = "";

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT imeUser, prezimeUser FROM user WHERE idUser='" + userId + "'");

        try {
            while (rs.next()) {
                userNameAndSurname = rs.getString("imeUser") + " " + rs.getString("prezimeUser");
            }
        } catch (SQLException e) {
            // HANDLEOVATI!!!
        }
        return userNameAndSurname;

    }

    public String[] getUserRowByID(){
        String resultArray[] = new String[6];
        resultArray[0] = idUser.getValue();

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM user WHERE idUser = '" + idUser.getValue() + "'");

        try {
            while (rs.next()) {
                resultArray[1] = (rs.getString("imeUser"));
                resultArray[2] = (rs.getString("prezimeUser"));
                resultArray[3] = (rs.getString("username"));
                resultArray[4] = (rs.getString("password"));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
        return resultArray;
    }

    public static String getUserPasswordByID(String idUser){
        DatabaseClass db = new DatabaseClass();
        String password = "";
        ResultSet rs = db.select("SELECT password FROM user WHERE idUser = '" + idUser + "'");

        try {
            while (rs.next()) {
                password = rs.getString("password");

            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
        return password;
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------SETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------

    public void deleteUser(){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement delete = db.exec("DELETE FROM user WHERE idUser=?");
            delete.setString(1, idUser.getValue());
            delete.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(UserModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public static void changeUserPassword(String idUser, String password){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement delete = db.exec("UPDATE user SET password = ? WHERE idUser=?");
            delete.setString(1, password);
            delete.setString(2, idUser);
            delete.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(UserModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}