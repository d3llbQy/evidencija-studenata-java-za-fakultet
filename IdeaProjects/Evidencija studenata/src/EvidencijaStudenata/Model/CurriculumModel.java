package EvidencijaStudenata.Model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CurriculumModel {
    SimpleStringProperty idPredmet = new SimpleStringProperty();
    SimpleStringProperty nazivPredmet = new SimpleStringProperty();

    public CurriculumModel(String idPredmet, String nazivPredmet){
        this.idPredmet = new SimpleStringProperty(idPredmet);
        this.nazivPredmet = new SimpleStringProperty(nazivPredmet);
    }

    public String getIdPredmet() {
        return idPredmet.get();
    }

    public String getNazivPredmet() {
        return nazivPredmet.get();
    }

//-------------------------------------------------------------------------------------------------------
//------------------------------------------------GETTERI------------------------------------------------
//-------------------------------------------------------------------------------------------------------

    public static ObservableList<CurriculumModel> listCurriculum() {
        ObservableList<CurriculumModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM predmet");

        try {
            while (rs.next()) {
                list.add(new CurriculumModel(rs.getString("idPredmet"), rs.getString("nazivPredmet")));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static ObservableList<CurriculumModel> listCurriculumForSpecificProfessor(String idUser) {
        ObservableList<CurriculumModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM predmet WHERE idPredmet IN (SELECT predmet_id FROM predmet_profesor WHERE profesor_id = '" + idUser + "')");

        try {
            while (rs.next()) {
                list.add(new CurriculumModel(rs.getString("idPredmet"), rs.getString("nazivPredmet")));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public String[] getCurriculumRowByID() {
        String resultArray[] = new String[2];
        resultArray[0] = idPredmet.getValue();

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM predmet WHERE idPredmet = '" + idPredmet.getValue() + "'");

        try {
            while (rs.next()) {
                resultArray[1] = (rs.getString("nazivPredmet"));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
        return resultArray;
    }


    //-------------------------------------------------------------------------------------------------------
//------------------------------------------------SETTERI------------------------------------------------
//-------------------------------------------------------------------------------------------------------


    public void deleteCurriculum(){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement delete = db.exec("DELETE FROM predmet WHERE idPredmet=?");
            delete.setString(1, idPredmet.getValue());
            delete.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(CurriculumModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
