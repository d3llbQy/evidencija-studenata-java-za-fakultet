package EvidencijaStudenata.Model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.String.valueOf;

public class IspitModel {
    SimpleIntegerProperty idIspit = new SimpleIntegerProperty();
    SimpleStringProperty nazivIspit = new SimpleStringProperty();
    SimpleStringProperty datumIspit = new SimpleStringProperty();

    public IspitModel(int idispit, String nazivispit, String datumispit){
        this.idIspit = new SimpleIntegerProperty(idispit);
        this.nazivIspit = new SimpleStringProperty(nazivispit);
        this.datumIspit = new SimpleStringProperty(datumispit);
    }

    public int getIdIspit() {
        return idIspit.get();
    }
    public String getNazivIspit() {
        return nazivIspit.get();
    }
    public String getDatumIspit() {
        return datumIspit.get();
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------GETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------

    public static ObservableList<IspitModel> listIspitByCurrId(String currId) {
        ObservableList<IspitModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM ispit WHERE predmet_id = '" + currId + "'");

        try {
            while (rs.next()) {
                list.add(new IspitModel(rs.getInt("idIspit"), rs.getString("nazivIspit"), rs.getString("datumIspit")));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public String[] getIspitRowByID(){
        String resultArray[] = new String[3];
        resultArray[0] = valueOf(idIspit.getValue());

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM ispit WHERE idIspit = '" + idIspit.getValue() + "'");

        try {
            while (rs.next()) {
                resultArray[1] = (rs.getString("nazivIspit"));
                resultArray[2] = (rs.getString("datumIspit"));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
        return resultArray;
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------SETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------

    public void deleteIspit(){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement delete = db.exec("DELETE FROM ispit WHERE idIspit=?");
            delete.setInt(1, idIspit.getValue());
            delete.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(IspitModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}