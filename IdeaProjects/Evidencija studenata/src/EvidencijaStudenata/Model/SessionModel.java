package EvidencijaStudenata.Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SessionModel {
    String idSession;
    String active;
    String user_id;

    public SessionModel(String idSession, String active, String user_id){
        this.idSession = idSession;
        this.active = active;
        this.user_id = user_id;
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------GETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    public static String getUserIdWithActiveSession(){
        String userId = "";

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select(("SELECT user_id FROM session WHERE active = 'Y'"));

        try {
            while (rs.next()) {
                userId = rs.getString("user_id");
            }
        } catch (SQLException e) {
            // HANDLEOVATI!!!
        }
        return userId;
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------SETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    public void insertIntoSession(){
        DatabaseClass db = new DatabaseClass();

        PreparedStatement insert = db.exec("INSERT INTO session (idSession, active, user_id) VALUES (?, ?, ?)");

        try{
            insert.setString(1, idSession);
            insert.setString(2, active);
            insert.setString(3, user_id);

            insert.executeUpdate();
        }catch (SQLException e){
            Logger.getLogger(SessionModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public static void setSessionToInnactive(){
        DatabaseClass db = new DatabaseClass();

        PreparedStatement update = db.exec("UPDATE session SET active='N' WHERE active='Y'");

        try{
            update.executeUpdate();
        }catch (SQLException e){
            Logger.getLogger(SessionModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
