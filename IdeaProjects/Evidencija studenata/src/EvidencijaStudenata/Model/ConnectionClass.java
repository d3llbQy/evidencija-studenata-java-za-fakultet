package EvidencijaStudenata.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionClass {
    private String host;
    private String username;
    private String password;
    private String database;

    protected Connection connection;

    public ConnectionClass(){
        this.host = "localhost";
        this.username = "root";
        this.password = "password";
        this.database = "evidencija";
        connect();
    }

    public void connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database + "?" + "user=" + username + "&password" + password + "autoReconnect=true&useSSL=false");
        } catch (ClassNotFoundException e) {

        } catch (SQLException e) {

        }
    }

    public void disconnect(){
        try{
            connection.close();
        } catch (SQLException e){

        }
    }

}
