package EvidencijaStudenata.Model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.String.valueOf;

public class UpisaniPredmetIspitModel {
    SimpleIntegerProperty id_ispit_upisani_predmet;
    SimpleStringProperty ocjenaIspit;
    SimpleIntegerProperty ispit_id;
    SimpleIntegerProperty upisani_predmet_id;
    SimpleStringProperty idBrIndexStudent;
    SimpleStringProperty nameAndSurnameStudent;
    SimpleStringProperty nazivIspit;

    public UpisaniPredmetIspitModel(int id_ispit_upisani_predmet, String ocjenaIspit, int ispit_id, int upisani_predmet_id, String idBrIndexStudent,
                                       String nameAndSurnameStudent, String nazivIspit){
        this.id_ispit_upisani_predmet = new SimpleIntegerProperty(id_ispit_upisani_predmet);
        this.ocjenaIspit = new SimpleStringProperty(ocjenaIspit);
        this.ispit_id = new SimpleIntegerProperty(ispit_id);
        this.upisani_predmet_id = new SimpleIntegerProperty(upisani_predmet_id);
        this.idBrIndexStudent = new SimpleStringProperty(idBrIndexStudent);
        this.nameAndSurnameStudent = new SimpleStringProperty(nameAndSurnameStudent);
        this.nazivIspit = new SimpleStringProperty(nazivIspit);
    }

    public int getId_ispit_upisani_predmet() {
        return id_ispit_upisani_predmet.get();
    }

    public String getOcjenaIspit() {
        return ocjenaIspit.get();
    }

    public int getIspit_id() {
        return ispit_id.get();
    }

    public int getUpisani_predmet_id() {
        return upisani_predmet_id.get();
    }

    public String getIdBrIndexStudent() {
        return idBrIndexStudent.get();
    }

    public String getNameAndSurnameStudent() {
        return nameAndSurnameStudent.get();
    }

    public String getNazivIspit() {
        return nazivIspit.get();
    }

    public static ObservableList<UpisaniPredmetIspitModel> upisaniPredmetIspitList(String studijska_grupa_id, int ispit_id){
        ObservableList<UpisaniPredmetIspitModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT stud.idBrIndexSTudent, stud.imeStudent, stud.prezimeStudent, kup.id_ispit_upisani_predmet, "+
                "kup.ocjenaIspit, kup.upisani_predmet_id FROM student stud, ispit_upisani_predmet kup " +
                "WHERE stud.idBrIndexStudent IN " +
                "(SELECT student_id FROM upisani_predmet WHERE idUpisaniPredmet = kup.upisani_predmet_id) AND " +
                "kup.ispit_id = " + ispit_id + " AND stud.studijska_grupa_id = '" + studijska_grupa_id + "'");

        try {
            while (rs.next()) {
                list.add(new UpisaniPredmetIspitModel((rs.getInt("kup.id_ispit_upisani_predmet")), (rs.getString("kup.ocjenaIspit")),
                        ispit_id, (rs.getInt("kup.upisani_predmet_id")), (rs.getString("stud.idBrIndexStudent")),
                        (rs.getString("stud.imeStudent") + " " + (rs.getString("stud.prezimeStudent"))), null));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static ObservableList<UpisaniPredmetIspitModel> upisaniPredmetIspitListByStudentID(String idBrIndexStudent, String predmet_id){
        ObservableList<UpisaniPredmetIspitModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT kol.nazivIspit, kup.id_ispit_upisani_predmet, kup.ocjenaIspit, kup.upisani_predmet_id, kup.ispit_id " +
                "FROM ispit kol, ispit_upisani_predmet kup WHERE kup.upisani_predmet_id = " +
                "(SELECT idUpisaniPredmet FROM upisani_predmet WHERE student_id = '" + idBrIndexStudent + "' AND predmet_id = '" + predmet_id + "') " +
                "AND kol.idIspit = kup.ispit_id");
        try {
            while (rs.next()) {
                list.add(new UpisaniPredmetIspitModel((rs.getInt("kup.id_ispit_upisani_predmet")), (rs.getString("kup.ocjenaIspit")),
                        (rs.getInt("kup.ispit_id")), (rs.getInt("kup.upisani_predmet_id")), idBrIndexStudent,
                        null, (rs.getString("kol.nazivIspit"))));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public String[] getUPIRowByID(){
        String resultArray[] = new String[2];
        resultArray[0] = valueOf(id_ispit_upisani_predmet.getValue());

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM ispit_upisani_predmet WHERE id_ispit_upisani_predmet = " + valueOf(id_ispit_upisani_predmet.getValue()));

        try {
            while (rs.next()) {
                resultArray[1] = (rs.getString("ocjenaIspit"));

            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
        return resultArray;
    }

}
