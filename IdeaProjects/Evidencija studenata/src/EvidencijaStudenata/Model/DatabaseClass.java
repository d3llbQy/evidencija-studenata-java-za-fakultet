package EvidencijaStudenata.Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseClass extends ConnectionClass {
    private Statement query;
    private PreparedStatement execQuery;

    public DatabaseClass(){ super();}

    public ResultSet select (String sql){
        try{
            query = connection.createStatement();
            return query.executeQuery(sql);
        } catch(SQLException e){
            System.out.println("Greska prilikom izvrsavanja upita " + e.getMessage());
            return null;
        }
    }

    public PreparedStatement exec(String sql){
        try{
            execQuery = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            return execQuery;
        } catch (SQLException e){
            System.out.println("Nije uspjelo izvrsavanje upita " + e.getMessage());
        }
        return null;
    }
}
