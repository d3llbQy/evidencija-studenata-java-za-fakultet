package EvidencijaStudenata.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddToTableModel {
    private SimpleStringProperty idTM = new SimpleStringProperty();
    private SimpleStringProperty nameTM = new SimpleStringProperty();
    public static String last_inserted_id = "";

    public AddToTableModel(String idTM, String nameTM){
        this.idTM = new SimpleStringProperty(idTM);
        this.nameTM = new SimpleStringProperty(nameTM);

    }

    public String getIdTM() {
        return idTM.get();
    }

    public String getNameTM() {
        return nameTM.get();
    }

    public static ObservableList<AddToTableModel> listForeignKeyData(String query, String idColumnLabel, String nameColumnLabel) {
        ObservableList<AddToTableModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select(query);

        try {
            while (rs.next()) {
                list.add(new AddToTableModel(rs.getString(idColumnLabel), rs.getString(nameColumnLabel)));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static ObservableList<AddToTableModel> listMToMData(String query, String idColumnLabel, String nameColumnLabel, String surnameColumnLabel) {
        ObservableList<AddToTableModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select(query);

        try {
            while (rs.next()) {
                // surameColumnLabel NIJE PRAZAN STRING KADA SE UZIMA IME I PREZIME PROFESORA I SPAJA U JEDNO
                if(!surnameColumnLabel.equals("")) {
                    list.add(new AddToTableModel(rs.getString(idColumnLabel), rs.getString(nameColumnLabel) + " " + rs.getString(surnameColumnLabel)));
                } else {
                    list.add(new AddToTableModel(rs.getString(idColumnLabel), rs.getString(nameColumnLabel)));
                }
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static void executeTableQuery(String insertString, String[] values, int quantity, int flag){
        DatabaseClass db = new DatabaseClass();
        PreparedStatement insert = db.exec(insertString);

try{
    if(quantity == 2){
        if(flag == 1) {
            insert.setString(1, values[0]);
            insert.setString(2, values[1]);
        } else if (flag == 2){
            insert.setString(1, values[1]);
            insert.setString(2, values[0]);
        }
        insert.executeUpdate();
    } else if (quantity == 3){
        if(flag == 1) {
            insert.setString(1, values[0]);
            insert.setString(2, values[1]);
            insert.setString(3, values[2]);
        } else if (flag == 2){
        insert.setString(1, values[1]);
        insert.setString(2, values[2]);
        insert.setString(3, values[0]);
    }
        insert.executeUpdate();
    } else if (quantity == 4){
        if(flag == 1) {
            insert.setString(1, values[0]);
            insert.setString(2, values[1]);
            insert.setString(3, values[2]);
            insert.setString(4, values[3]);
        } else if (flag == 2){
            insert.setString(1, values[1]);
            insert.setString(2, values[2]);
            insert.setString(3, values[3]);
            insert.setString(4, values[0]);
        }
        insert.executeUpdate();
    } else if (quantity == 5){
        if(flag == 1) {
            insert.setString(1, values[0]);
            insert.setString(2, values[1]);
            insert.setString(3, values[2]);
            insert.setString(4, values[3]);
            insert.setString(5, values[4]);
        } else if (flag == 2){
            insert.setString(1, values[1]);
            insert.setString(2, values[2]);
            insert.setString(3, values[3]);
            insert.setString(4, values[4]);
            insert.setString(5, values[0]);
        }
        insert.executeUpdate();
    } else if (quantity == 6){
        if(flag == 1) {
            insert.setString(1, values[0]);
            insert.setString(2, values[1]);
            insert.setString(3, values[2]);
            insert.setString(4, values[3]);
            insert.setString(5, values[4]);
            insert.setString(6, values[5]);
        } else if (flag == 2){
            insert.setString(1, values[1]);
            insert.setString(2, values[2]);
            insert.setString(3, values[3]);
            insert.setString(4, values[4]);
            insert.setString(5, values[5]);
            insert.setString(6, values[0]);
        }
        insert.executeUpdate();
    }
} catch (SQLException e){
    Logger.getLogger(AddToTableModel.class.getName()).log(Level.SEVERE, null, e);
}

        ResultSet rs = db.select("SELECT LAST_INSERT_ID();");

        try {
            while (rs.next()) {
                last_inserted_id = (rs.getString(1));

            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
    }

}
