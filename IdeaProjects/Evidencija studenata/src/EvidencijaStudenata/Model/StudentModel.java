package EvidencijaStudenata.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudentModel {
    SimpleStringProperty idBrIndexStudent = new SimpleStringProperty();
    SimpleStringProperty imeStudent = new SimpleStringProperty();
    SimpleStringProperty prezimeStudent = new SimpleStringProperty();
    SimpleStringProperty datumRodjenja = new SimpleStringProperty();
    SimpleStringProperty mjestoRodjenja = new SimpleStringProperty();
    SimpleStringProperty studijskaGrupa = new SimpleStringProperty();

    public StudentModel(String idBrIndexStudent, String imeStudent, String prezimeStudent, String datumRodjenjaUser, String mjestoRodjenja, String studijskaGrupa) {
        this.idBrIndexStudent = new SimpleStringProperty(idBrIndexStudent);
        this.imeStudent = new SimpleStringProperty(imeStudent);
        this.prezimeStudent = new SimpleStringProperty(prezimeStudent);
        this.datumRodjenja = new SimpleStringProperty(datumRodjenjaUser);
        this.mjestoRodjenja = new SimpleStringProperty(mjestoRodjenja);
        this.studijskaGrupa = new SimpleStringProperty(studijskaGrupa);
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------GETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------

    public String getIdBrIndexStudent() {
        return idBrIndexStudent.get();
    }

    public String getImeStudent() {
        return imeStudent.get();
    }

    public String getPrezimeStudent() {
        return prezimeStudent.get();
    }

    public String getDatumRodjenja() {
        return datumRodjenja.get();
    }

    public String getMjestoRodjenja() {
        return mjestoRodjenja.get();
    }

    public String getStudijskaGrupa() {
        return studijskaGrupa.get();
    }

    public static ObservableList<StudentModel> listStudent() {
        ObservableList<StudentModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT s.idBrIndexStudent, s.imeStudent, s.prezimeStudent, s.datumRodjenja, s.mjestoRodjenja, sg.nazivStudijskaGrupa FROM student s, studijska_grupa sg " +
                "WHERE sg.idStudijskaGrupa = s.studijska_grupa_id");

        try {
            while (rs.next()) {
                list.add(new StudentModel(rs.getString("s.idBrIndexStudent"), rs.getString("s.imeStudent"), rs.getString("s.prezimeStudent"),
                        rs.getString("s.datumRodjenja"), rs.getString("s.mjestoRodjenja"), rs.getString("sg.nazivStudijskaGrupa")));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public static ObservableList<StudentModel> getStudentsByStudGrpID(String studijska_grupa_id){
        ObservableList<StudentModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM student WHERE studijska_grupa_id = '" + studijska_grupa_id + "'");

        try {
            while (rs.next()) {
                list.add(new StudentModel(rs.getString("idBrIndexStudent"), rs.getString("imeStudent"), rs.getString("prezimeStudent"),
                        rs.getString("datumRodjenja"), rs.getString("mjestoRodjenja"), null));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public String[] getStudentRowByID(){
        String resultArray[] = new String[6];
        resultArray[0] = idBrIndexStudent.getValue();

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM student WHERE idBrIndexStudent = '" + idBrIndexStudent.getValue() + "'");

        try {
            while (rs.next()) {
                resultArray[1] = (rs.getString("imeStudent"));
                resultArray[2] = (rs.getString("prezimeStudent"));
                resultArray[3] = (rs.getString("datumRodjenja"));
                resultArray[4] = (rs.getString("mjestoRodjenja"));
                resultArray[5] = (rs.getString("studijska_grupa_id"));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
        return resultArray;
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------SETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    public void deleteStudent(){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement delete = db.exec("DELETE FROM student WHERE idBrIndexStudent=?");
            delete.setString(1, idBrIndexStudent.getValue());
            delete.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(StudentModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}