package EvidencijaStudenata.Model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.String.valueOf;

public class KolokvijModel {
    SimpleIntegerProperty idKolokvij = new SimpleIntegerProperty();
    SimpleStringProperty nazivKolokvij = new SimpleStringProperty();
    SimpleStringProperty datumKolokvij = new SimpleStringProperty();

    public KolokvijModel(int idKolokvij, String nazivKolokvij, String datumKolokvij){
        this.idKolokvij = new SimpleIntegerProperty(idKolokvij);
        this.nazivKolokvij = new SimpleStringProperty(nazivKolokvij);
        this.datumKolokvij = new SimpleStringProperty(datumKolokvij);
    }

    public int getIdKolokvij() {
        return idKolokvij.get();
    }
    public String getNazivKolokvij() {
        return nazivKolokvij.get();
    }
    public String getDatumKolokvij() {
        return datumKolokvij.get();
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------GETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------

    public static ObservableList<KolokvijModel> listKolokvijByCurrId(String currId) {
        ObservableList<KolokvijModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM kolokvij WHERE predmet_id = '" + currId + "'");

        try {
            while (rs.next()) {
                list.add(new KolokvijModel(rs.getInt("idKolokvij"), rs.getString("nazivKolokvij"), rs.getString("datumKolokvij")));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }

        return list;
    }

    public String[] getKolokvijRowByID(){
        String resultArray[] = new String[3];
        resultArray[0] = valueOf(idKolokvij.getValue());

        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM kolokvij WHERE idKolokvij = '" + idKolokvij.getValue() + "'");

        try {
            while (rs.next()) {
                resultArray[1] = (rs.getString("nazivKolokvij"));
                resultArray[2] = (rs.getString("datumKolokvij"));
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
        return resultArray;
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------SETTERI------------------------------------------------
    //-------------------------------------------------------------------------------------------------------

    public void deleteKolokvij(){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement delete = db.exec("DELETE FROM kolokvij WHERE idKolokvij=?");
            delete.setInt(1, idKolokvij.getValue());
            delete.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(KolokvijModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
