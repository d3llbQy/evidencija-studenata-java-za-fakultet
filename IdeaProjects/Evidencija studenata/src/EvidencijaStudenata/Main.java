package EvidencijaStudenata;

import EvidencijaStudenata.Controller.LoginScreenController;
import EvidencijaStudenata.Model.SessionModel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("EvidencijaStudenata/View/LoginScreen.fxml"));
        root = loader.load();
        LoginScreenController lsc = loader.getController();
        lsc.start(root, primaryStage);
    }

    @Override
    public void stop(){
        SessionModel.setSessionToInnactive();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
