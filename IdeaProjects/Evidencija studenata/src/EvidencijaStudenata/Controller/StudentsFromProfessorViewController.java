package EvidencijaStudenata.Controller;

import EvidencijaStudenata.AddToTableOpener;
import EvidencijaStudenata.Model.IspitModel;
import EvidencijaStudenata.Model.StudentModel;
import EvidencijaStudenata.Model.UpisaniPredmetIspitModel;
import EvidencijaStudenata.Model.UpisaniPredmetKolokvijModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.print.DocFlavor;

public class StudentsFromProfessorViewController extends InitializeCall{
    @FXML
    TableView<StudentModel> studentTable;
    @FXML
    TableColumn<StudentModel, String> idBrIndexStudent;
    @FXML
    TableColumn<StudentModel, String> imeStudent;
    @FXML
    TableColumn<StudentModel, String> prezimeStudent;
    @FXML
    TableView<UpisaniPredmetKolokvijModel> kolokvijTable;
    @FXML
    TableColumn<UpisaniPredmetKolokvijModel, String> kolokvijNazivTableCol;
    @FXML
    TableColumn<UpisaniPredmetKolokvijModel, String> brojBodovaKolokvijTableCol;
    @FXML
    Button addKolokvijPointsButton;
    @FXML
    TableView<UpisaniPredmetIspitModel> ispitTable;
    @FXML
    TableColumn<UpisaniPredmetIspitModel, String> ispitNazivTableCol;
    @FXML
    TableColumn<UpisaniPredmetIspitModel, String> ocjenaIspitTableCol;
    @FXML
    Button addOcjenaIspitButton;

    private String studijska_grupa_id;
    private String predmet_id;
    private StudentModel lastSelection;

    private String viewName = "EvidencijaStudenata/View/AddToTableView.fxml";
    private String[] labelsTextK = {"ID:", "Broj bodova:"};
    private String captionEditK = "Uredi bodove na kolokviju";
    private String editSQLK = "UPDATE kolokvij_upisani_predmet SET brojBodovaKolokvij = ? WHERE id_kolokvij_upisani_predmet = ?";

    private String[] labelsTextI = {"ID:", "Ocjena:"};
    private String captionEditI = "Uredi ocjenu na ispitu";
    private String editSQLI = "UPDATE ispit_upisani_predmet SET ocjenaIspit = ? WHERE id_ispit_upisani_predmet = ?";

    public void initStudentsFromProfessorView(String studijska_grupa_id, String predmet_id){
        this.studijska_grupa_id = studijska_grupa_id;
        this.predmet_id = predmet_id;

        addKolokvijPointsButton.setDisable(true);
        addOcjenaIspitButton.setDisable(true);

        initStudentTable();
    }

    private void initStudentTable(){
        idBrIndexStudent.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("idBrIndexStudent"));
        imeStudent.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("imeStudent"));
        prezimeStudent.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("prezimeStudent"));

        studentTable.setItems(StudentModel.getStudentsByStudGrpID(studijska_grupa_id));

        studentTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null){
                lastSelection = newSelection;
                kolokvijNazivTableCol.setCellValueFactory(new PropertyValueFactory<UpisaniPredmetKolokvijModel, String>("nazivKolokvij"));
                brojBodovaKolokvijTableCol.setCellValueFactory(new PropertyValueFactory<UpisaniPredmetKolokvijModel, String>("brojBodovaKolokvij"));
                kolokvijTable.setItems(UpisaniPredmetKolokvijModel.upisaniPredmetKolokvijListByStudentID(newSelection.getIdBrIndexStudent(), predmet_id));

                ispitNazivTableCol.setCellValueFactory(new PropertyValueFactory<UpisaniPredmetIspitModel, String>("nazivIspit"));
                ocjenaIspitTableCol.setCellValueFactory(new PropertyValueFactory<UpisaniPredmetIspitModel, String>("ocjenaIspit"));
                ispitTable.setItems(UpisaniPredmetIspitModel.upisaniPredmetIspitListByStudentID(newSelection.getIdBrIndexStudent(), predmet_id));
            }
        });

        if(lastSelection != null){
            studentTable.getSelectionModel().select(lastSelection);
        }

        kolokvijTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null){
                addKolokvijPointsButton.setDisable(false);
            } else {
                addKolokvijPointsButton.setDisable(true);
            }
        });

        ispitTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null){
                addOcjenaIspitButton.setDisable(false);
            } else {
                addOcjenaIspitButton.setDisable(true);
            }
        });
    }

    // CLICK HANDLERI
    public void onAddKolokvijPointsButtonClick(ActionEvent actionEvent) {
        openAddKolokvijPoints();
    }

    public void onAddOcjenaIspitButtonClick(ActionEvent actionEvent) {
        openAddIspitGrade();
    }

    private void openAddKolokvijPoints(){
        UpisaniPredmetKolokvijModel upkm = kolokvijTable.getSelectionModel().getSelectedItem();
        String[] selectedData = upkm.getUPKRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsTextK, viewName, captionEditK, editSQLK, this, selectedData, false, null);
    }

    private void openAddIspitGrade(){
        UpisaniPredmetIspitModel upim = ispitTable.getSelectionModel().getSelectedItem();
        String[] selectedData = upim.getUPIRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsTextI, viewName, captionEditI, editSQLI, this, selectedData, false, null);
    }


    @Override
    void callInitialize() {
        initStudentsFromProfessorView(studijska_grupa_id, predmet_id);
    }

    @Override
    void manyToManyInsert(String last_inserted_id, String querySQL) {

    }
}
