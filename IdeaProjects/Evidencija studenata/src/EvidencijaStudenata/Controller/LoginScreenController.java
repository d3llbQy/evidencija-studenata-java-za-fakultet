package EvidencijaStudenata.Controller;

import EvidencijaStudenata.Model.SessionModel;
import EvidencijaStudenata.Model.UserModel;
import javafx.event.ActionEvent;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyCode;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.event.KeyEvent;
import sun.rmi.runtime.Log;


public class LoginScreenController implements Initializable{
    @FXML
    AnchorPane anchorPane;
    @FXML
    Label errorLabel;
    @FXML
    TextField usernameTextField;
    @FXML
    PasswordField passwordField;
    @FXML
    Button loginButton;

    public void start(Parent root, Stage primaryStage){
        primaryStage.setTitle("Evidencija studenata");
        primaryStage.setScene(new Scene(root, 700, 500));
        primaryStage.show();

        loginButton.getScene().addEventHandler(javafx.scene.input.KeyEvent.KEY_PRESSED, new EventHandler<javafx.scene.input.KeyEvent>() {
            @Override
            public void handle(javafx.scene.input.KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER){
                    doLogin();
                }
            }
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle rb){

    }

    // LOGIN BUTTON CLICK
    public void onLoginButtonClick(ActionEvent actionEvent) {
        doLogin();
    }

    private void doLogin(){

        Scene scene = loginButton.getScene();
        if (usernameTextField.getText().equals("") || passwordField.getText().equals("")) {
            setErrorLabel("Morate unijeti korisničko ime i lozinku!");
            return;
        }

        if (credentialsVerified()) {
            deletePossibleActiveSessions();
            setSession();
            openMainMenu();
        } else{
            setErrorLabel("Korisničko ime ili lozinka su pogrešni!");
        }
    }

    private boolean credentialsVerified() {
        // VRACA TRUE AKO JE ISPUNJEN UVJET
        return (UserModel.getUserPassword(usernameTextField.getText()).equals(passwordField.getText()));
    }

    private void setSession(){
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy:HH:mm:ss");
        Date date = new Date();

        SessionModel sessionModel = new SessionModel(df.format(date),"Y", UserModel.getUserId(usernameTextField.getText()));
        sessionModel.insertIntoSession();
    }

    // U PRAVILU NE BI TREBALO BIT AKTIVNIH SESIJA, ALI ZA SVAKI SLUCAJ SE BRISU
    private void deletePossibleActiveSessions(){
        SessionModel.setSessionToInnactive();
    }

    private void openMainMenu(){
        try {
            Parent root;
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("EvidencijaStudenata/View/MainMenuView.fxml"));
            root = loader.load();
            MainMenuViewController mmvc = loader.getController();
            mmvc.onEnter(UserModel.getUserRole(SessionModel.getUserIdWithActiveSession()));

            Stage stage = new Stage();
            stage.setTitle("Prikaz svih kontakata u Bazi podataka");
            stage.setScene(new Scene(root, 1000, 500));
            stage.show();

            loginButton.getScene().getWindow().hide();

        } catch (IOException ex) {
            Logger.getLogger(LoginScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setErrorLabel(String errorLabelText){
        errorLabel.setVisible(true);
        errorLabel.setText(errorLabelText);
        errorLabel.setStyle("-fx-text-fill: red;");
    }
}

