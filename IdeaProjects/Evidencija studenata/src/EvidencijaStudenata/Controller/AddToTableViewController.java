package EvidencijaStudenata.Controller;

import EvidencijaStudenata.Model.AddToTableModel;
import EvidencijaStudenata.Model.DatabaseClass;
import EvidencijaStudenata.Model.StudyGroupModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.Node;
import javafx.util.StringConverter;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AddToTableViewController {
    @FXML
    Label titleLabel;
    @FXML
    Label title1;
    @FXML
    Label title2;
    @FXML
    Label title3;
    @FXML
    Label title4;
    @FXML
    Label title5;
    @FXML
    Label title6;
    @FXML
    Label title7;
    @FXML
    TextField textField1;
    @FXML
    TextField textField2;
    @FXML
    TextField textField3;
    @FXML
    TextField textField4;
    @FXML
    TextField textField5;
    @FXML
    TextField textField6;
    @FXML
    ComboBox<AddToTableModel> comboBox1;

    private String querySQL;
    private int mode; // VARIJABLA KOJA OZNACAUJE KOLIKO VARIJABLI CE SE INSERTAT
    private String idForForeignKey; // ID ZA STRANI KLJUC KOJI SE EKSTRAKTIRA IZ ODABRANE VRIJEDNOSTI COMBOBOXA
    private String[] selectedData;
    private InitializeCall object;
    private boolean hasForeignKey;

    public void initLabelsAndTextFields(String[]labelsText, int labelsQuantity, String caption, String querySQL, InitializeCall object, String[] selectedData,
                                        boolean hasForeignKey, ObservableList<AddToTableModel> list){
        this.querySQL = querySQL;
        this.object = object;
        this.selectedData = selectedData;
        this.hasForeignKey = hasForeignKey;
        titleLabel.setText(caption);

        if(hasForeignKey){
            comboBox1.setItems(list);

            if(selectedData == null) {
                comboBox1.getSelectionModel().selectFirst();
            } else {
                setComboBoxDataWhenHasForeign();
            }
        }

        // 2 LABELA
        if (labelsQuantity == 2){
            mode = 2;

            title3.setVisible(false);

            textField3.setVisible(false);

            title1.setText(labelsText[0]);

            if(hasForeignKey){
                title7.setVisible(true);
                comboBox1.setVisible(true);
                title7.setText(labelsText[1]);

                title2.setVisible(false);
                textField2.setVisible(false);

                setComboBoxConverterAndListener();
            } else {
                title2.setText(labelsText[1]);
            }

            // Ako je edit mod, a ne add
            if (selectedData != null){
                textField1.setDisable(true);

                textField1.setText(selectedData[0]);
                textField2.setText(selectedData[1]);
            }
            // 3 LABELA
        } else if(labelsQuantity == 3){
            mode = 3;

            title1.setText(labelsText[0]);
            title2.setText(labelsText[1]);

            if(hasForeignKey){
                title7.setVisible(true);
                comboBox1.setVisible(true);
                title7.setText(labelsText[2]);

                title3.setVisible(false);
                textField3.setVisible(false);

                setComboBoxConverterAndListener();
            } else {
                title3.setText(labelsText[2]);
            }

            if (selectedData != null){
                textField1.setDisable(true);

                textField1.setText(selectedData[0]);
                textField2.setText(selectedData[1]);
                textField3.setText(selectedData[2]);
            }
            // 4 LABELA
        } else if (labelsQuantity == 4){
            mode = 4;

            title4.setVisible(true);

            textField4.setVisible(true);

            title1.setText(labelsText[0]);
            title2.setText(labelsText[1]);
            title3.setText(labelsText[2]);

            if(hasForeignKey){
                title7.setVisible(true);
                comboBox1.setVisible(true);
                title7.setText(labelsText[3]);

                title4.setVisible(false);
                textField4.setVisible(false);

                setComboBoxConverterAndListener();
            } else {
                title4.setText(labelsText[3]);
            }

            if (selectedData != null){
                textField1.setDisable(true);

                textField1.setText(selectedData[0]);
                textField2.setText(selectedData[1]);
                textField3.setText(selectedData[2]);
                textField4.setText(selectedData[3]);
            }
            // 5 LABELA
        } else if (labelsQuantity == 5){
            mode = 5;

            title4.setVisible(true);
            title5.setVisible(true);

            textField4.setVisible(true);
            textField5.setVisible(true);

            title1.setText(labelsText[0]);
            title2.setText(labelsText[1]);
            title3.setText(labelsText[2]);
            title4.setText(labelsText[3]);

            if(hasForeignKey){
                title7.setVisible(true);
                comboBox1.setVisible(true);
                title7.setText(labelsText[4]);

                title4.setVisible(false);
                textField4.setVisible(false);

                setComboBoxConverterAndListener();
            }else {
                title5.setText(labelsText[4]);
            }

            if (selectedData != null){
                textField1.setDisable(true);

                textField1.setText(selectedData[0]);
                textField2.setText(selectedData[1]);
                textField3.setText(selectedData[2]);
                textField4.setText(selectedData[3]);
                textField5.setText(selectedData[4]);
            }
            // 6 LABELA
        } else if (labelsQuantity == 6){
            mode = 6;

            title4.setVisible(true);
            title5.setVisible(true);
            title6.setVisible(true);

            textField4.setVisible(true);
            textField5.setVisible(true);
            textField6.setVisible(true);

            title1.setText(labelsText[0]);
            title2.setText(labelsText[1]);
            title3.setText(labelsText[2]);
            title4.setText(labelsText[3]);
            title5.setText(labelsText[4]);

            if (hasForeignKey) {
                title7.setVisible(true);
                comboBox1.setVisible(true);
                title7.setText(labelsText[5]);

                title6.setVisible(false);
                textField6.setVisible(false);

                setComboBoxConverterAndListener();
            }else {
                title6.setText(labelsText[5]);
            }

            if (selectedData != null){
                textField1.setDisable(true);

                textField1.setText(selectedData[0]);
                textField2.setText(selectedData[1]);
                textField3.setText(selectedData[2]);
                textField4.setText(selectedData[3]);
                textField5.setText(selectedData[4]);
                textField6.setText(selectedData[5]);
            }
        }
    }

    private void setComboBoxConverterAndListener(){
        comboBox1.setConverter(new StringConverter<AddToTableModel>() {
            @Override
            public String toString(AddToTableModel object) {
                return object.getNameTM();
            }

            @Override
            public AddToTableModel fromString(String string) {
                return comboBox1.getItems().stream().filter(sg ->
                sg.getNameTM().equals(string)).findFirst().orElse(null);
            }
        });

        comboBox1.valueProperty().addListener((obs, oldval, newval) -> {
            if(newval != null){
                idForForeignKey = newval.getIdTM();
            }
        });
    }

    private void setComboBoxDataWhenHasForeign(){
        String id = selectedData[selectedData.length - 1];
        int i = 0;
        int index = 0;

        ObservableList<AddToTableModel> cbitems = comboBox1.getItems();
        for (AddToTableModel tm : cbitems){
            i++;
            if (tm.getIdTM().equals(id)){
                index = i-1;
                idForForeignKey = tm.getIdTM();
            }
        }
        comboBox1.getSelectionModel().select(index);

    }

    // BUTTON CLICK LISTENER
    public void onAddButtonClick(ActionEvent actionEvent) {
        String[] textFieldValues = new String[mode];

        switch(mode){
            case 2:
                textFieldValues[0] = textField1.getText();

                if(hasForeignKey){
                    textFieldValues[1] = idForForeignKey;
                    break;
                }else{
                    textFieldValues[1] = textField2.getText();
                    break;
                }
            case 3:
                textFieldValues[0] = textField1.getText();
                textFieldValues[1] = textField2.getText();

                if(hasForeignKey){
                    textFieldValues[2] = idForForeignKey;
                    break;
                }else{
                    textFieldValues[2] = textField3.getText();
                    break;
                }
            case 4:
                textFieldValues[0] = textField1.getText();
                textFieldValues[1] = textField2.getText();
                textFieldValues[2] = textField3.getText();

                if(hasForeignKey){
                    textFieldValues[3] = idForForeignKey;
                    break;
                }else{
                    textFieldValues[3] = textField4.getText();
                    break;
                }
            case 5:
                textFieldValues[0] = textField1.getText();
                textFieldValues[1] = textField2.getText();
                textFieldValues[2] = textField3.getText();
                textFieldValues[3] = textField4.getText();

                if(hasForeignKey){
                    textFieldValues[4] = idForForeignKey;
                    break;
                }else{
                    textFieldValues[4] = textField5.getText();
                    break;
                }
            case 6:
                textFieldValues[0] = textField1.getText();
                textFieldValues[1] = textField2.getText();
                textFieldValues[2] = textField3.getText();
                textFieldValues[3] = textField4.getText();
                textFieldValues[4] = textField5.getText();

                if(hasForeignKey){
                        textFieldValues[5] = idForForeignKey;
                        break;
                }else{
                    textFieldValues[5] = textField6.getText();
                    break;
                }
        }

        if(selectedData != null) {
            AddToTableModel.executeTableQuery(querySQL, textFieldValues, textFieldValues.length, 2);
        } else {
            AddToTableModel.executeTableQuery(querySQL, textFieldValues, textFieldValues.length, 1);
        }



        object.callInitialize();

        if(selectedData == null) {
            object.manyToManyInsert(AddToTableModel.last_inserted_id, querySQL);
        }

        ((Node)(actionEvent.getSource())).getScene().getWindow().hide();
    }

}
