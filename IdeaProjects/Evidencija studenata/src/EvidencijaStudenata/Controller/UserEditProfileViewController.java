package EvidencijaStudenata.Controller;

import EvidencijaStudenata.Model.SessionModel;
import EvidencijaStudenata.Model.UserModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

public class UserEditProfileViewController {
    @FXML
    Label errorLabel;
    @FXML
    PasswordField repeatedNewPassword;
    @FXML
    PasswordField newPassword;
    @FXML
    PasswordField oldPassword;

    public void onConfirmPasswordChangeButton(ActionEvent actionEvent) {
        errorLabel.setStyle("-fx-text-fill: red;");
        if(!UserModel.getUserPasswordByID(SessionModel.getUserIdWithActiveSession()).equals(oldPassword.getText())){

            setErrorLabel("Stara lozinka nije ispravna!", false);
        } else if(newPassword.getText().equals("") && repeatedNewPassword.getText().equals("")){
            setErrorLabel("Niste unijeli novu lozinku!", false);
        }
        else if(!(newPassword.getText().equals(repeatedNewPassword.getText()))){
            setErrorLabel("Nova lozinka i ponovljena nova lozinka se ne poklapaju!", false);
        } else{
            changePasswordInDB();
            setErrorLabel("Uspješno ste promijenili lozinku!", true);
        }
    }

    private void setErrorLabel(String errorLabelText, boolean successfulChange){
        errorLabel.setText(errorLabelText);

        if(successfulChange){
            errorLabel.setStyle("-fx-text-fill: white;");

            oldPassword.setText("");
            newPassword.setText("");
            repeatedNewPassword.setText("");
        } else{
            errorLabel.setStyle("-fx-text-fill: red;");
        }

        errorLabel.setVisible(true);
    }

    private void changePasswordInDB(){
        UserModel.changeUserPassword(SessionModel.getUserIdWithActiveSession(), newPassword.getText());
    }
}
