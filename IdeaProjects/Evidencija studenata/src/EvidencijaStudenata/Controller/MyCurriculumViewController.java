package EvidencijaStudenata.Controller;

import EvidencijaStudenata.Model.CurriculumModel;
import EvidencijaStudenata.Model.SessionModel;
import EvidencijaStudenata.Model.StudyGroupModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyCurriculumViewController implements Initializable{
    @FXML
    TableView<CurriculumModel> curriculumTable;
    @FXML
    TableColumn<CurriculumModel, String> nazivPredmetCol;

    @FXML
    TableView<StudyGroupModel> studGrpTable;
    @FXML
    TableColumn<StudyGroupModel, String> nazivStudijskaGrupaCol;

    @FXML
    Button studentsViewButton;
    @FXML
    Button currPropertiesViewButton;


    @Override
    public void initialize(URL url, ResourceBundle rb){
        ObservableList<CurriculumModel> data = CurriculumModel.listCurriculumForSpecificProfessor(SessionModel.getUserIdWithActiveSession());
        nazivPredmetCol.setCellValueFactory(new PropertyValueFactory<CurriculumModel, String>("nazivPredmet"));
        curriculumTable.setItems(data);

        studentsViewButton.setDisable(true);
        currPropertiesViewButton.setDisable(true);

        curriculumTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            setStudGrpTable(curriculumTable.getSelectionModel().getSelectedItem().getIdPredmet());

            if(newSelection != null){
                currPropertiesViewButton.setDisable(false);
            } else{
                currPropertiesViewButton.setDisable(true);
            }
        });

        studGrpTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null){
                studentsViewButton.setDisable(false);
            } else{
                studentsViewButton.setDisable(true);
            }
        });
    }

    private void setStudGrpTable(String currId){
        ObservableList<StudyGroupModel> data = StudyGroupModel.listStudyGroupForSpecificCurr(curriculumTable.getSelectionModel().getSelectedItem().getIdPredmet());

        nazivStudijskaGrupaCol.setCellValueFactory(new PropertyValueFactory<StudyGroupModel, String>("nazivStudijskaGrupa"));
        studGrpTable.setItems(data);
    }

    //CLICK HANDLERI
    public void onStudentsViewButtonClick(ActionEvent actionEvent) {
        openStudentsViewScene();
    }
    public void onCurrPropertiesViewButtonClick(ActionEvent actionEvent) {
        openCurriculumPropertiesViewScene();
    }

    private void openStudentsViewScene(){
        String fxmlPath = "EvidencijaStudenata/View/StudentsFromProfessorView.fxml";
        String windowTitle = "Pregled studenata u studijskoj grupi " + studGrpTable.getSelectionModel().getSelectedItem().getNazivStudijskaGrupa();
        try {
            Parent root;
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(fxmlPath));
            root = loader.load();

            StudentsFromProfessorViewController sfpvc = loader.getController();
            sfpvc.initStudentsFromProfessorView(studGrpTable.getSelectionModel().getSelectedItem().getIdStudijskaGrupa(),
                    curriculumTable.getSelectionModel().getSelectedItem().getIdPredmet());

            Stage stage = new Stage();
            stage.setTitle(windowTitle);
            stage.setScene(new Scene(root, 1000, 841));
            stage.show();

            //((Node)(actionEvent.getSource())).getScene().getWindow().hide();

        } catch (IOException ex) {
            Logger.getLogger(LoginScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void openCurriculumPropertiesViewScene(){
        String fxmlPath = "EvidencijaStudenata/View/CurriculumPropertiesView.fxml";
        String windowTitle = "Pregled značajki u predmetu " + curriculumTable.getSelectionModel().getSelectedItem().getNazivPredmet();
        try {
            Parent root;
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(fxmlPath));
            root = loader.load();

            CurriculumPropertiesViewController sfpvc = loader.getController();
            sfpvc.initCurriculumPropertiesView(curriculumTable.getSelectionModel().getSelectedItem().getIdPredmet());

            Stage stage = new Stage();
            stage.setTitle(windowTitle);
            stage.setScene(new Scene(root, 1000, 500));
            stage.show();

            //((Node)(actionEvent.getSource())).getScene().getWindow().hide();

        } catch (IOException ex) {
            Logger.getLogger(LoginScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }


