package EvidencijaStudenata.Controller;

import EvidencijaStudenata.AddToTableOpener;
import EvidencijaStudenata.Model.AddToTableModel;
import EvidencijaStudenata.Model.CurriculumModel;
import EvidencijaStudenata.Model.UserModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CurriculumViewController extends InitializeCall implements Initializable{
    @FXML
    TableView<CurriculumModel> curriculumTable;
    @FXML
    TableColumn<CurriculumModel, String> idPredmetCol;
    @FXML
    TableColumn<CurriculumModel, String> nazivPredmetCol;
    @FXML
    Button editCurrButton;
    @FXML
    Button deleteCurrButton;
    @FXML
    Button addProfessorToCurrButton;
    @FXML
    Button addStudGrpToCurrButton;

    private String[] labelsText = {"ID:", "Naziv predmeta:"};
    private String viewName = "EvidencijaStudenata/View/AddToTableView.fxml";
    private String captionInsert = "Dodaj novi predmet";
    private String captionEdit = "Uredi predmet";
    private String insertSQL = "INSERT INTO predmet (idPredmet, nazivPredmet) VALUES(?,?)";
    private String editSQL = "UPDATE predmet SET nazivPredmet = ? WHERE idPredmet = ?";

    private CurriculumModel cm;
    private String windowTitle;
    private String mTmQueryIN;
    private String mTmQueryOUT;
    private String onAddQuery;
    private String onDeleteQuery;
    private String idColumnLabel;
    private String nameColumnLabel;
    private String surNameColumnLabel;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        editCurrButton.setDisable(true);
        deleteCurrButton.setDisable(true);
        addProfessorToCurrButton.setDisable(true);
        addStudGrpToCurrButton.setDisable(true);

        ObservableList<CurriculumModel> data = CurriculumModel.listCurriculum();
        idPredmetCol.setCellValueFactory(new PropertyValueFactory<CurriculumModel, String>("idPredmet"));
        nazivPredmetCol.setCellValueFactory(new PropertyValueFactory<CurriculumModel, String>("nazivPredmet"));
        curriculumTable.setItems(data);

        curriculumTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                editCurrButton.setDisable(false);
                deleteCurrButton.setDisable(false);
                addProfessorToCurrButton.setDisable(false);
                addStudGrpToCurrButton.setDisable(false);
            } else {
                editCurrButton.setDisable(true);
                deleteCurrButton.setDisable(true);
                addProfessorToCurrButton.setDisable(true);
                addStudGrpToCurrButton.setDisable(true);
            }
        });
    }

    // BUTTON CLICK HANDLERI
    public void onAddCurrClick(ActionEvent actionEvent) {
        openAddToTableView();
    }

    public void onEditCurrClick(ActionEvent actionEvent) {
        openEditInTableView();
    }

    public void onDeleteCurrClick(ActionEvent actionEvent) {
        openDeleteDialog();
    }

    public void onAddProfessorToCurrButton(ActionEvent actionEvent) {
        openAddProfessorToCurrScene();
    }

    public void onAddStudGrpToCurrButton(ActionEvent actionEvent) {
        openAddStudGrpToCurrScene();
    }
//----------------------------------------------------------------------------------------------------------------------------------------------------------------

    // POZIVACI METODA KLASE AddToTableViewController
    private void openAddToTableView(){
        AddToTableOpener atto = new AddToTableOpener();

        atto.openAndInitAddToTableView(labelsText, viewName, captionInsert, insertSQL, this, null, false, null);
    }

    private void openEditInTableView(){
        CurriculumModel cm = curriculumTable.getSelectionModel().getSelectedItem();
        String[] selectedData = cm.getCurriculumRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsText, viewName, captionEdit, editSQL, this, selectedData, false, null);
    }

    private void openDeleteDialog(){
        ButtonType ok = new ButtonType("U redu", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Odustani", ButtonBar.ButtonData.CANCEL_CLOSE);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Potvrda brisanja");
        alert.setHeaderText("Označili ste predmet za brisanje");
        alert.setContentText("Želite li stvarno to učiniti?");

        alert.getButtonTypes().setAll(ok, cancel);

        Optional<ButtonType> result = alert.showAndWait();

        // AKO SE KLIKNE NA U REDU
        if(result.get() == ok){
            CurriculumModel cm = curriculumTable.getSelectionModel().getSelectedItem();
            cm.deleteCurriculum();
            initialize(null, null);
        }
    }

    private void openAddProfessorToCurrScene(){
        cm = curriculumTable.getSelectionModel().getSelectedItem();
        windowTitle = "Pregled profesora na predmetu " + cm.getNazivPredmet();
        mTmQueryIN = "SELECT * FROM user " +
                "WHERE idUser IN (SELECT profesor_id FROM predmet_profesor WHERE predmet_id = '" + cm.getIdPredmet() + "') AND rolaUser = 'profesor'";
        mTmQueryOUT = "SELECT * FROM user " +
                "WHERE idUser NOT IN (SELECT profesor_id FROM predmet_profesor WHERE predmet_id = '" + cm.getIdPredmet() + "') AND rolaUser = 'profesor'";
        onAddQuery = "INSERT INTO predmet_profesor (predmet_id, profesor_id) VALUES (?, ?)";
        onDeleteQuery = "DELETE FROM predmet_profesor WHERE predmet_id = ? AND profesor_id = ?";
        idColumnLabel = "idUser";
        nameColumnLabel = "imeUser";
        surNameColumnLabel = "prezimeUser";

        openSceneWithInitializatorVars(cm, windowTitle, mTmQueryIN, mTmQueryOUT, onAddQuery, onDeleteQuery, idColumnLabel, nameColumnLabel, surNameColumnLabel);
    }

    private void openAddStudGrpToCurrScene(){
        cm = curriculumTable.getSelectionModel().getSelectedItem();
        windowTitle = "Pregled studijskih grupa na predmetu " + cm.getNazivPredmet();
        mTmQueryIN = "SELECT * FROM studijska_grupa " +
                "WHERE idStudijskaGrupa IN (SELECT studijska_grupa_id FROM predmet_studijska_grupa WHERE predmet_id = '" + cm.getIdPredmet() + "')";
        mTmQueryOUT = "SELECT * FROM studijska_grupa " +
                "WHERE idStudijskaGrupa NOT IN (SELECT studijska_grupa_id FROM predmet_studijska_grupa WHERE predmet_id = '" + cm.getIdPredmet() + "')";
        onAddQuery = "INSERT INTO predmet_studijska_grupa (predmet_id, studijska_grupa_id) VALUES (?, ?)";
        onDeleteQuery = "DELETE FROM predmet_studijska_grupa WHERE predmet_id = ? AND studijska_grupa_id = ?";
        idColumnLabel = "idStudijskaGrupa";
        nameColumnLabel = "nazivStudijskaGrupa";
        surNameColumnLabel = "";

        openSceneWithInitializatorVars(cm, windowTitle, mTmQueryIN, mTmQueryOUT, onAddQuery, onDeleteQuery, idColumnLabel, nameColumnLabel, surNameColumnLabel);
    }

    private void openSceneWithInitializatorVars(CurriculumModel cm, String windowTitle, String mTmQueryIN, String mTmQueryOUT, String onAddQuery, String onDeleteQuery,
                                                String idColumnLabel, String nameColumnLabel, String surNameColumnLabel){
        String fxmlPath = "EvidencijaStudenata/View/ManyToManyTableView.fxml";
        try {
            Parent root;
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(fxmlPath));
            root = loader.load();

            ManyToManyTableViewController mtmtvc = loader.getController();
            mtmtvc.initManyToManyTableView(cm.getIdPredmet(), cm.getNazivPredmet(),mTmQueryIN, mTmQueryOUT, onAddQuery, onDeleteQuery, idColumnLabel, nameColumnLabel, surNameColumnLabel);

            Stage stage = new Stage();
            stage.setTitle(windowTitle);
            stage.setScene(new Scene(root, 1000, 500));
            stage.show();

            //((Node)(actionEvent.getSource())).getScene().getWindow().hide();

        } catch (IOException ex) {
            Logger.getLogger(LoginScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // IMPLEMENTACIJA IZ InitializeCall APSTRAKTNE KLASE
    @Override
    public void callInitialize(){
        initialize(null, null);
    }

    @Override
    public void manyToManyInsert(String id, String querySQL){

    }

}
