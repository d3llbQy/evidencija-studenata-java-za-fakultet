package EvidencijaStudenata.Controller;

public abstract class InitializeCall {
    abstract void callInitialize();
    abstract void manyToManyInsert(String last_inserted_id, String querySQL);
}
