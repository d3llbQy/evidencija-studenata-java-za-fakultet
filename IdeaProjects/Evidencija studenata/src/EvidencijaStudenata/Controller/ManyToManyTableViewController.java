package EvidencijaStudenata.Controller;

import EvidencijaStudenata.Model.AddToTableModel;
import EvidencijaStudenata.Model.DatabaseClass;
import EvidencijaStudenata.Model.StudentModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;

import java.sql.PreparedStatement;
import java.sql.SQLException;


public class ManyToManyTableViewController {
    @FXML
    TableView<AddToTableModel> contentTable;
    @FXML
    TableColumn<AddToTableModel, String> idColumn;
    @FXML
    TableColumn<AddToTableModel, String> nameColumn;
    @FXML
    ComboBox<AddToTableModel> contentComboBox;
    @FXML
    Label labelTitle;
    @FXML
    Button addButton;
    @FXML
    Button deleteButton;

    private String idFromScene;
    private String name;
    private String mTmQueryIN;
    private String mTmQueryOUT;
    private String onAddQuery;
    private String onDeleteQuery;
    private String idColumnLabel;
    private String nameColumnLabel;
    private String surnameColumnLabel;

    private String idFromComboBox;

    public void initManyToManyTableView(String idFromScene, String name, String mTmQueryIN, String mTmQueryOUT, String onAddQuery, String onDeleteQuery,
                                        String idColumnLabel, String nameColumnLabel, String surnameColumnLabel){
        this.idFromScene = idFromScene;
        this.name = name;
        this.mTmQueryIN = mTmQueryIN;
        this.mTmQueryOUT = mTmQueryOUT;
        this.onAddQuery = onAddQuery;
        this.onDeleteQuery = onDeleteQuery;
        this.idColumnLabel = idColumnLabel;
        this.nameColumnLabel = nameColumnLabel;
        this.surnameColumnLabel = surnameColumnLabel;

        addButton.setDisable(true);
        deleteButton.setDisable(true);

        contentTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null) {
                deleteButton.setDisable(false);
            }
        });

        contentComboBox.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) ->{
            if(newSelection != null) {
                addButton.setDisable(false);
            }
        });

        labelTitle.setText(name);

        initContentTable();
        initComboBox();
    }

    private void initContentTable(){
        idColumn.setCellValueFactory(new PropertyValueFactory<AddToTableModel, String>("idTM"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<AddToTableModel, String>("nameTM"));
        contentTable.setItems(AddToTableModel.listMToMData(mTmQueryIN, idColumnLabel, nameColumnLabel, surnameColumnLabel));
    }

    private void initComboBox(){
        contentComboBox.setItems(AddToTableModel.listMToMData(mTmQueryOUT, idColumnLabel, nameColumnLabel, surnameColumnLabel));
        setComboBoxConverterAndListener();
    }

    private void setComboBoxConverterAndListener(){
        contentComboBox.setConverter(new StringConverter<AddToTableModel>() {
            @Override
            public String toString(AddToTableModel object) {
                return object.getNameTM();
            }

            @Override
            public AddToTableModel fromString(String string) {
                return contentComboBox.getItems().stream().filter(sg ->
                        sg.getNameTM().equals(string)).findFirst().orElse(null);
            }
        });

        contentComboBox.valueProperty().addListener((obs, oldval, newval) -> {
            if(newval != null){
                idFromComboBox = newval.getIdTM();
            }
        });
    }

    // CLICK HANDLERI
    public void onAddButtonClick(ActionEvent actionEvent) {
        doManyToManyTableInsert();
        signUpOrOutStudentsForCurriculum("INSERT INTO upisani_predmet (predmet_id, student_id) VALUES (?, ?)", "INSERT");
        initManyToManyTableView(idFromScene, name, mTmQueryIN, mTmQueryOUT, onAddQuery, onDeleteQuery, idColumnLabel, nameColumnLabel, surnameColumnLabel);
    }

    public void onDeleteButtonClick(ActionEvent actionEvent) {
        doManyToManyTableDelete();
        signUpOrOutStudentsForCurriculum("DELETE FROM upisani_predmet WHERE predmet_id = ? AND student_id = ?", "DELETE");
        initManyToManyTableView(idFromScene, name, mTmQueryIN, mTmQueryOUT, onAddQuery, onDeleteQuery, idColumnLabel, nameColumnLabel, surnameColumnLabel);
    }

    private void doManyToManyTableInsert(){
        DatabaseClass db = new DatabaseClass();
        PreparedStatement action = db.exec(onAddQuery);;

        try {
            action.setString(1, idFromScene);
            action.setString(2, idFromComboBox);
            action.executeUpdate();
        } catch (SQLException e){

        }
    }

    private void doManyToManyTableDelete(){
        DatabaseClass db = new DatabaseClass();
        PreparedStatement action = db.exec(onDeleteQuery);;

        try {
            action.setString(1, idFromScene);
            action.setString(2, contentTable.getSelectionModel().getSelectedItem().getIdTM());
            action.executeUpdate();
        } catch (SQLException e){

        }
    }

    private void signUpOrOutStudentsForCurriculum(String sqlStatement, String mode){
        ObservableList<StudentModel> list;
        if(mode.equals("INSERT")) {
            list = StudentModel.getStudentsByStudGrpID((contentComboBox.getSelectionModel().getSelectedItem().getIdTM()));
        } else {
            list = StudentModel.getStudentsByStudGrpID((contentTable.getSelectionModel().getSelectedItem().getIdTM()));
        }

        list.stream().forEach((student -> {
            doSignUpOrOut(student.getIdBrIndexStudent(), sqlStatement);
        }));
    }

    private void doSignUpOrOut(String idBrIndexStudent, String sqlStatement){
        DatabaseClass db = new DatabaseClass();
        PreparedStatement action = db.exec(sqlStatement);

        try {
            action.setString(1, idFromScene);
            action.setString(2, idBrIndexStudent);
            action.executeUpdate();
        } catch (SQLException e){

        }
    }

}
