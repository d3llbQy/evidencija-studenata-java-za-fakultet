package EvidencijaStudenata.Controller;

        import EvidencijaStudenata.AddToTableOpener;
        import EvidencijaStudenata.Model.CurriculumModel;
        import EvidencijaStudenata.Model.UserModel;
        import javafx.collections.ObservableList;
        import javafx.event.ActionEvent;
        import javafx.fxml.FXML;
        import javafx.fxml.Initializable;
        import javafx.scene.control.*;
        import javafx.scene.control.cell.PropertyValueFactory;
        import javafx.scene.input.MouseButton;

        import java.awt.event.MouseEvent;
        import java.net.URL;
        import java.util.Optional;
        import java.util.ResourceBundle;

public class ProfessorViewController extends InitializeCall implements Initializable{
    private String[] labelsText = {"ID:", "Ime profesora:", "Prezime profesora:", "Korisničko ime:", "Lozinka:"};
    private String viewName = "EvidencijaStudenata/View/AddToTableView.fxml";
    private String captionInsert = "Dodaj novog profesora";
    private String captionEdit = "Uredi profesora";
    private String insertSQL = "INSERT INTO user (idUser,imeUser,prezimeUser,username,password,rolaUser) VALUES(?,?,?,?,?,'profesor')";
    private String editSQL = "UPDATE user SET imeUser=?,prezimeUser=?,username=?,password=? WHERE idUser=?";

    @FXML
    TableView<UserModel> professorTable;
    @FXML
    TableColumn<UserModel, String> professorId;
    @FXML
    TableColumn<UserModel, String> professorSurname;
    @FXML
    TableColumn<UserModel, String> professorName;
    @FXML
    Button editProfButton;
    @FXML
    Button deleteProfButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        editProfButton.setDisable(true);
        deleteProfButton.setDisable(true);

        ObservableList<UserModel> data = UserModel.listProfessor();
        professorId.setCellValueFactory(new PropertyValueFactory<UserModel, String>("idUser"));
        professorSurname.setCellValueFactory(new PropertyValueFactory<UserModel, String>("prezimeUser"));
        professorName.setCellValueFactory(new PropertyValueFactory<UserModel, String>("imeUser"));

        professorTable.setItems(data);

        professorTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                editProfButton.setDisable(false);
                deleteProfButton.setDisable(false);
            } else {
                editProfButton.setDisable(true);
                deleteProfButton.setDisable(true);
            }
        });
    }

    // BUTTON CLICK HANDLERI
    public void onAddProfClick(ActionEvent actionEvent) {
        openAddToTableView();
    }

    public void onEditProfClick(ActionEvent actionEvent) {
        openEditInTableView();
    }
    public void onDeleteProfClick(ActionEvent actionEvent) {
        openDeleteDialog();
    }

    private void openAddToTableView(){
        AddToTableOpener atto = new AddToTableOpener();

        atto.openAndInitAddToTableView(labelsText, viewName, captionInsert, insertSQL, this, null, false, null);
    }

    private void openEditInTableView(){
        UserModel um = professorTable.getSelectionModel().getSelectedItem();
        String[] selectedData = um.getUserRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsText, viewName, captionEdit, editSQL, this, selectedData, false, null);
    }


    private void openDeleteDialog(){
        ButtonType ok = new ButtonType("U redu", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Odustani", ButtonBar.ButtonData.CANCEL_CLOSE);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Potvrda brisanja");
        alert.setHeaderText("Označili ste profesora za brisanje");
        alert.setContentText("Želite li stvarno to učiniti?");

        alert.getButtonTypes().setAll(ok, cancel);

        Optional<ButtonType> result = alert.showAndWait();

        // AKO SE KLIKNE NA U REDU
        if(result.get() == ok){
            UserModel um = professorTable.getSelectionModel().getSelectedItem();
            um.deleteUser();
            initialize(null, null);
        }
    }

    @Override
    public void callInitialize(){
        initialize(null,null);
    }

    @Override
    public void manyToManyInsert(String id, String querySQL){

    }
}
