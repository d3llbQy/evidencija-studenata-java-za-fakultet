package EvidencijaStudenata.Controller;

import EvidencijaStudenata.AddToTableOpener;
import EvidencijaStudenata.Model.IspitModel;
import EvidencijaStudenata.Model.StudyGroupModel;
import EvidencijaStudenata.Model.UpisaniPredmetIspitModel;
import EvidencijaStudenata.Model.UserModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class IspitViewController extends InitializeCall{
    @FXML
    TableView<StudyGroupModel> studGrpOnIspitTable;
    @FXML
    TableColumn<StudyGroupModel, String> studGrpNameTableCol;
    @FXML
    TableView<UpisaniPredmetIspitModel> ispitTable;
    @FXML
    TableColumn<UpisaniPredmetIspitModel, String> studentNameAndSurnameTableCol;
    @FXML
    TableColumn<UpisaniPredmetIspitModel, String> gradeOnIspitNumberTableCol;
    @FXML
    Button addIspitPointsButton;

    private int ispitID;
    private String currID;
    private String[] labelsText = {"ID:", "Ocjena:"};
    private String viewName = "EvidencijaStudenata/View/AddToTableView.fxml";
    private String captionEdit = "Uredi ocjenu na ispitu";
    private String editSQL = "UPDATE ispit_upisani_predmet SET ocjenaIspit = ? WHERE id_ispit_upisani_predmet = ?";

    private StudyGroupModel lastSelection;

    public void initIspitView(int ispitID, String currID){
        this.ispitID = ispitID;
        this.currID = currID;

        addIspitPointsButton.setDisable(true);

        initStudyGroupTable();
    }

    private void initStudyGroupTable(){
        ObservableList<StudyGroupModel> list = StudyGroupModel.listStudyGroupForSpecificCurr(currID);

        studGrpNameTableCol.setCellValueFactory(new PropertyValueFactory<StudyGroupModel, String>("nazivStudijskaGrupa"));
        studGrpOnIspitTable.setItems(list);

        studGrpOnIspitTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null) {
                lastSelection = newSelection;
                ObservableList<UpisaniPredmetIspitModel> listKol = UpisaniPredmetIspitModel.upisaniPredmetIspitList(newSelection.getIdStudijskaGrupa(), ispitID);

                studentNameAndSurnameTableCol.setCellValueFactory(new PropertyValueFactory<UpisaniPredmetIspitModel, String>("nameAndSurnameStudent"));
                gradeOnIspitNumberTableCol.setCellValueFactory(new PropertyValueFactory<UpisaniPredmetIspitModel, String>("ocjenaIspit"));
                ispitTable.setItems(listKol);
            }

        });

        if(lastSelection != null){
            studGrpOnIspitTable.getSelectionModel().select(lastSelection);
        }

        ispitTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null){
                addIspitPointsButton.setDisable(false);
            } else {
                addIspitPointsButton.setDisable(true);
            }
        });
    }

    // CLICK HANDLERI

    public void onAddIspitGradeButtonClick(ActionEvent actionEvent) {
        openAddIspitGrade();
    }

    private void openAddIspitGrade(){
        UpisaniPredmetIspitModel upkm = ispitTable.getSelectionModel().getSelectedItem();
        String[] selectedData = upkm.getUPIRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsText, viewName, captionEdit, editSQL, this, selectedData, false, null);
    }

    @Override
    public void callInitialize(){
        initIspitView(ispitID, currID);
    }

    @Override
    public void manyToManyInsert(String last_inserted_id, String querySQL){

    }
}
