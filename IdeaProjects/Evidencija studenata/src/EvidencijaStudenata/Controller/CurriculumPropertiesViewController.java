package EvidencijaStudenata.Controller;

import EvidencijaStudenata.AddToTableOpener;
import EvidencijaStudenata.Model.DatabaseClass;
import EvidencijaStudenata.Model.IspitModel;
import EvidencijaStudenata.Model.KolokvijModel;
import EvidencijaStudenata.Model.UserModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CurriculumPropertiesViewController extends InitializeCall implements Initializable{
    @FXML
    TableView<KolokvijModel> kolokvijTable;
    @FXML
    TableColumn<KolokvijModel, String> nazivKolokvijTableColumn;
    @FXML
    TableColumn<KolokvijModel, String> datumKolokvijTableColumn;
    @FXML
    Button editKolokvijButton;
    @FXML
    Button deleteKolokvijButton;
    @FXML
    Button evidKolokvijButton;
    @FXML
    TableView<IspitModel> ispitTable;
    @FXML
    TableColumn<IspitModel, String> nazivIspitTableColumn;
    @FXML
    TableColumn<IspitModel, String> datumIspitTableColumn;
    @FXML
    Button editIspitButton;
    @FXML
    Button deleteIspitButton;
    @FXML
    Button evidIspitButton;

    private String viewName = "EvidencijaStudenata/View/AddToTableView.fxml";
    private String[] labelsTextInsertK = {"Naziv kolokvija:", "Datum kolokvija:"};
    private String[] labelsTextUpdateK = {"ID:", "Naziv kolokvija:", "Datum kolokvija:"};
    private String captionInsertK = "Dodaj novi kolokvij";
    private String captionEditK = "Uredi kolokvij";
    private String editSQLK = "UPDATE kolokvij SET nazivKolokvij=?,datumKolokvij=? WHERE idKolokvij=?";

    private String[] labelsTextInsertI = {"Naziv ispita:", "Datum ispita:"};
    private String[] labelsTextUpdateI = {"ID:", "Naziv ispita:", "Datum ispita:"};
    private String captionInsertI = "Dodaj novi ispit";
    private String captionEditI = "Uredi ispit";
    private String editSQLI = "UPDATE ispit SET nazivIspit=?,datumIspit=? WHERE idIspit=?";
    private String currentCurriculumID;
    private String insertSQLK;
    private String insertSQLI;
    private boolean isFirstEnterInScene = true;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (!isFirstEnterInScene){
            initCurriculumPropertiesView(currentCurriculumID);
        }
    }

    public void initCurriculumPropertiesView(String curriculumId) {
        isFirstEnterInScene = false;

        this.currentCurriculumID = curriculumId;
        insertSQLK = "INSERT INTO kolokvij (nazivKolokvij,datumKolokvij, predmet_id) VALUES(?,?, '" + currentCurriculumID + "')";
        insertSQLI = "INSERT INTO ispit (nazivIspit, datumIspit, predmet_id) VALUES(?,?,'" + currentCurriculumID + "')";

        initKolokvijTable();
        initIspitTable();
    }

    private void initKolokvijTable(){
        editKolokvijButton.setDisable(true);
        deleteKolokvijButton.setDisable(true);
        evidKolokvijButton.setDisable(true);

        ObservableList<KolokvijModel> data = KolokvijModel.listKolokvijByCurrId(currentCurriculumID);
        nazivKolokvijTableColumn.setCellValueFactory(new PropertyValueFactory<KolokvijModel, String>("nazivKolokvij"));
        datumKolokvijTableColumn.setCellValueFactory(new PropertyValueFactory<KolokvijModel, String>("datumKolokvij"));

        kolokvijTable.setItems(data);

        kolokvijTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                editKolokvijButton.setDisable(false);
                deleteKolokvijButton.setDisable(false);
                evidKolokvijButton.setDisable(false);
            } else {
                editKolokvijButton.setDisable(true);
                deleteKolokvijButton.setDisable(true);
                evidKolokvijButton.setDisable(true);
            }
        });
    }

    private void initIspitTable(){
        editIspitButton.setDisable(true);
        deleteIspitButton.setDisable(true);
        evidIspitButton.setDisable(true);

        ObservableList<IspitModel> data = IspitModel.listIspitByCurrId(currentCurriculumID);
        nazivIspitTableColumn.setCellValueFactory(new PropertyValueFactory<IspitModel, String>("nazivIspit"));
        datumIspitTableColumn.setCellValueFactory(new PropertyValueFactory<IspitModel, String>("datumIspit"));

        ispitTable.setItems(data);

        ispitTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                editIspitButton.setDisable(false);
                deleteIspitButton.setDisable(false);
                evidIspitButton.setDisable(false);
            } else {
                editIspitButton.setDisable(true);
                deleteIspitButton.setDisable(true);
                evidIspitButton.setDisable(true);
            }
        });
    }

    // CLICK HANDLERI
    public void onAddKolokvijButtonClick(ActionEvent actionEvent) {
        openAddToTableViewKol();
    }

    public void onEditKolokvijButtonClick(ActionEvent actionEvent) {
        openEditInTableViewKol();
    }

    public void onDeleteKolokvijButtonClick(ActionEvent actionEvent) {
        openDeleteDialogKol();
    }

    public void onEvidKolokvijButtonClick(ActionEvent actionEvent) {
        openKolokvijEvid(kolokvijTable.getSelectionModel().getSelectedItem().getIdKolokvij());
    }

    public void onAddIspitButtonClick(ActionEvent actionEvent) {
        openAddToTableViewIsp();
    }

    public void onEditIspitButtonClick(ActionEvent actionEvent) {
        openEditInTableViewIsp();
    }

    public void onDeleteIspitButtonClick(ActionEvent actionEvent) {
        openDeleteDialogIsp();
    }

    public void onEvidIspitButtonClick(ActionEvent actionEvent) {
        openIspitEvid(ispitTable.getSelectionModel().getSelectedItem().getIdIspit());
    }

    private void openAddToTableViewKol(){
        AddToTableOpener atto = new AddToTableOpener();

        atto.openAndInitAddToTableView(labelsTextInsertK, viewName, captionInsertK, insertSQLK, this, null, false, null);
    }

    private void openEditInTableViewKol(){
        KolokvijModel km = kolokvijTable.getSelectionModel().getSelectedItem();
        String[] selectedData = km.getKolokvijRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsTextUpdateK, viewName, captionEditK, editSQLK, this, selectedData, false, null);
    }

    private void openDeleteDialogKol(){
        ButtonType ok = new ButtonType("U redu", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Odustani", ButtonBar.ButtonData.CANCEL_CLOSE);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Potvrda brisanja");
        alert.setHeaderText("Označili ste kolokvij za brisanje");
        alert.setContentText("Želite li stvarno to učiniti?");

        alert.getButtonTypes().setAll(ok, cancel);

        Optional<ButtonType> result = alert.showAndWait();

        // AKO SE KLIKNE NA U REDU
        if(result.get() == ok){
            deleteKolokvijUpisaniPredmet(kolokvijTable.getSelectionModel().getSelectedItem().getIdKolokvij());

            KolokvijModel km = kolokvijTable.getSelectionModel().getSelectedItem();
            km.deleteKolokvij();

            initialize(null, null);
        }
    }

    private void openAddToTableViewIsp(){
        AddToTableOpener atto = new AddToTableOpener();

        atto.openAndInitAddToTableView(labelsTextInsertI, viewName, captionInsertI, insertSQLI, this, null, false, null);
    }

    private void openEditInTableViewIsp(){
        IspitModel im = ispitTable.getSelectionModel().getSelectedItem();
        String[] selectedData = im.getIspitRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsTextUpdateI, viewName, captionEditI, editSQLI, this, selectedData, false, null);
    }

    private void openDeleteDialogIsp() {
        ButtonType ok = new ButtonType("U redu", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Odustani", ButtonBar.ButtonData.CANCEL_CLOSE);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Potvrda brisanja");
        alert.setHeaderText("Označili ste ispit za brisanje");
        alert.setContentText("Želite li stvarno to učiniti?");

        alert.getButtonTypes().setAll(ok, cancel);

        Optional<ButtonType> result = alert.showAndWait();

        // AKO SE KLIKNE NA U REDU
        if (result.get() == ok) {
            deleteIspitUpisaniPredmet(ispitTable.getSelectionModel().getSelectedItem().getIdIspit());

            IspitModel im = ispitTable.getSelectionModel().getSelectedItem();
            im.deleteIspit();

            initialize(null, null);
        }
    }

    @Override
    public void callInitialize(){
        initialize(null,null);
    }

    @Override
    public void manyToManyInsert(String id, String querySQL){
        String tableName = returnTableName(querySQL);
        String insertTable = "";
        String insertColumn = "";

        if(tableName.equals("kolokvij")){
            insertTable = "kolokvij_upisani_predmet";
            insertColumn = "kolokvij_id";
        } else if (tableName.equals("ispit")){
            insertTable = "ispit_upisani_predmet";
            insertColumn = "ispit_id";
        }

        ObservableList<KolokvijModel> list = FXCollections.observableArrayList();
        DatabaseClass db = new DatabaseClass();
        ResultSet rs = db.select("SELECT * FROM upisani_predmet WHERE predmet_id = '" + currentCurriculumID + "'");

        try {
            while (rs.next()) {
                doManyToManyInsert(rs.getInt("idUpisaniPredmet"), id, insertTable, insertColumn);
            }
        } catch (SQLException e) {
            System.out.println("Greska prilikom iteracije " + e.getMessage());
        }
    }

    private void doManyToManyInsert(int upisani_predmet_id, String id, String insertTable, String insertColumn){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement insert = db.exec("INSERT INTO " + insertTable + "(upisani_predmet_id, " + insertColumn + ") VALUES (?, ?)");
            insert.setInt(1, upisani_predmet_id);
            insert.setInt(2, Integer.valueOf(id));
            insert.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(KolokvijModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private String returnTableName(String querySQL){
        String tableName = "";
        StringTokenizer st = new StringTokenizer(querySQL, " ");
        int i = 0;

        while(st.hasMoreTokens()){
            st.nextToken();
            i++;
            if(i == 2){
               return st.nextToken();
            }
        }
        return tableName;
    }

    private void deleteKolokvijUpisaniPredmet(int kolokvijId){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement insert = db.exec("DELETE FROM kolokvij_upisani_predmet WHERE kolokvij_id = " + String.valueOf(kolokvijId));
            insert.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(KolokvijModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void openKolokvijEvid(int selectedKolokvijID){
        String fxmlPath = "EvidencijaStudenata/View/KolokvijView.fxml";
        String windowTitle = "Pregled kolokvija " + kolokvijTable.getSelectionModel().getSelectedItem().getNazivKolokvij();
        try {
            Parent root;
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(fxmlPath));
            root = loader.load();

            KolokvijViewController kvc = loader.getController();
            kvc.initKolokvijView(kolokvijTable.getSelectionModel().getSelectedItem().getIdKolokvij(), currentCurriculumID);

            Stage stage = new Stage();
            stage.setTitle(windowTitle);
            stage.setScene(new Scene(root, 1000, 588));
            stage.show();

            //((Node)(actionEvent.getSource())).getScene().getWindow().hide();

        } catch (IOException ex) {
            Logger.getLogger(LoginScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void deleteIspitUpisaniPredmet(int ispitId){
        try{
            DatabaseClass db = new DatabaseClass();
            PreparedStatement insert = db.exec("DELETE FROM ispit_upisani_predmet WHERE ispit_id = " + String.valueOf(ispitId));
            insert.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(IspitModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void openIspitEvid(int selectedIspitID) {
        String fxmlPath = "EvidencijaStudenata/View/IspitView.fxml";
        String windowTitle = "Pregled ispita " + ispitTable.getSelectionModel().getSelectedItem().getNazivIspit();
        try {
            Parent root;
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(fxmlPath));
            root = loader.load();

            IspitViewController ivc = loader.getController();
            ivc.initIspitView(ispitTable.getSelectionModel().getSelectedItem().getIdIspit(), currentCurriculumID);

            Stage stage = new Stage();
            stage.setTitle(windowTitle);
            stage.setScene(new Scene(root, 1000, 588));
            stage.show();

            //((Node)(actionEvent.getSource())).getScene().getWindow().hide();

        } catch (IOException ex) {
            Logger.getLogger(LoginScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}

