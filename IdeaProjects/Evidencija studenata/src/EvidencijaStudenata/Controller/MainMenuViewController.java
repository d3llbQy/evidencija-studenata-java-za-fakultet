package EvidencijaStudenata.Controller;

import EvidencijaStudenata.Main;
import EvidencijaStudenata.Model.SessionModel;
import EvidencijaStudenata.Model.UserModel;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainMenuViewController extends Object{
    @FXML
    Button profesorViewButton;
    @FXML
    Button curriculumViewButton;
    @FXML
    Button myCurriculumViewButton;
    @FXML
    Button studyGroupsViewButton;
    @FXML
    Button studentsViewButton;
    @FXML
    Label nameAndSurnameLabel;

    private String viewFolderPath = "EvidencijaStudenata/View/";


    public void onEnter(String role) {
        setUIBasedOnRole(role);
    }

    private void setUIBasedOnRole(String role){
        setWelcomeLabel();

        if (role.equals("profesor")) {
            profesorViewButton.setVisible(false);
            curriculumViewButton.setVisible(false);
            studyGroupsViewButton.setVisible(false);
            studentsViewButton.setVisible(false);
        } else if(role.equals("admin")){
            myCurriculumViewButton.setVisible(false);
        }
    }

    private void setWelcomeLabel(){
        nameAndSurnameLabel.setText("Pozdrav, " + UserModel.getUserName(SessionModel.getUserIdWithActiveSession()));
    }


    // MENU CLICK HANDLERI
    public void onProfesorViewButtonClick(ActionEvent actionEvent) {
        ProfessorViewController pvc = new ProfessorViewController();
        openScene(actionEvent, viewFolderPath + "ProfessorView.fxml", "Prikaz svih profesora", pvc);
    }

    public void onCurriculumViewButtonClick(ActionEvent actionEvent) {
        CurriculumViewController ccc = new CurriculumViewController();
        openScene(actionEvent, viewFolderPath + "CurriculumChooseView.fxml", "Prikaz svih predmeta", ccc);
    }

    public void onStudyGroupsViewButtonClick(ActionEvent actionEvent) {
        StudyGroupViewController  sgvc = new StudyGroupViewController();
        openScene(actionEvent, viewFolderPath + "StudyGroupView.fxml", "Prikaz svih studijskih grupa", sgvc);
    }

    public void onMyCurriculumViewButtonClick(ActionEvent actionEvent) {
        StudyGroupViewController  sgvc = new StudyGroupViewController();
        openScene(actionEvent, viewFolderPath + "MyCurriculumView.fxml", "Prikaz svih mojih predmeta", sgvc);
    }

    public void onStudentViewButtonClick(ActionEvent actionEvent) {
        StudentViewController  svc = new StudentViewController();
        openScene(actionEvent, viewFolderPath + "StudentView.fxml", "Prikaz svih studenata", svc);
    }

    public void onProfileEditClick(ActionEvent actionEvent) {
        UserEditProfileViewController  uepvc = new UserEditProfileViewController();
        openScene(actionEvent, viewFolderPath + "UserEditProfileView.fxml", "Promjena lozinke", uepvc);
    }

    public void onLogoutClick(ActionEvent actionEvent) {
        doLogout(actionEvent);
    }

    private void openScene(ActionEvent actionEvent, String fxmlPath, String windowTitle, Object controllerObject){
        try {
            Parent root;
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(fxmlPath));
            root = loader.load();

            Stage stage = new Stage();
            stage.setTitle(windowTitle);
            stage.setScene(new Scene(root, 1000, 500));
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(LoginScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void doLogout(ActionEvent actionEvent){
        SessionModel.setSessionToInnactive();

        try{
            Parent root;
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("EvidencijaStudenata/View/LoginScreen.fxml"));
            root = loader.load();
            LoginScreenController lsc = loader.getController();
            lsc.start(root, new Stage());


            ((Node)(actionEvent.getSource())).getScene().getWindow().hide();

        } catch (IOException e){

        }


    }

}

