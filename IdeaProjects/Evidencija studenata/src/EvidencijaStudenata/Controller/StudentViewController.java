package EvidencijaStudenata.Controller;

import EvidencijaStudenata.AddToTableOpener;
import EvidencijaStudenata.Model.AddToTableModel;
import EvidencijaStudenata.Model.StudentModel;
import EvidencijaStudenata.Model.StudentModel;
import EvidencijaStudenata.Model.StudyGroupModel;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.*;

public class StudentViewController extends InitializeCall implements Initializable {
    private String[] labelsText = {"Br.Indeksa:", "Ime studenta:", "Prezime studenta:", "Datum rodjenja:", "Mjesto rodjenja:", "Studijska grupa"};
    private String viewName = "EvidencijaStudenata/View/AddToTableView.fxml";
    private String captionInsert = "Dodaj novog studenta";
    private String captionEdit = "Uredi studenta";
    private String insertSQL = "INSERT INTO student (idBrIndexStudent,imeStudent,prezimeStudent,datumRodjenja,mjestoRodjenja,studijska_grupa_id) VALUES(?,?,?,?,?,?)";
    private String editSQL = "UPDATE student SET imeStudent=?,prezimeStudent=?,datumRodjenja=?,mjestoRodjenja=?,studijska_grupa_id=? WHERE idBrIndexStudent=?";

    @FXML
    TableView<StudentModel> studentTable;
    @FXML
    TableColumn<StudentModel, String> idBrIndexStudent;
    @FXML
    TableColumn<StudentModel, String> imeStudent;
    @FXML
    TableColumn<StudentModel, String> prezimeStudent;
    @FXML
    TableColumn<StudentModel, String> datumRodjenja;
    @FXML
    TableColumn<StudentModel, String> mjestoRodjenja;
    @FXML
    TableColumn<StudentModel, String> studijskaGrupa;
    @FXML
    Button editStudentButton;
    @FXML
    Button deleteStudentButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        editStudentButton.setDisable(true);
        deleteStudentButton.setDisable(true);

        ObservableList<StudentModel> data = StudentModel.listStudent();
        idBrIndexStudent.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("idBrIndexStudent"));
        imeStudent.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("imeStudent"));
        prezimeStudent.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("prezimeStudent"));
        datumRodjenja.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("datumRodjenja"));
        mjestoRodjenja.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("mjestoRodjenja"));
        studijskaGrupa.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("studijskaGrupa"));

        studentTable.setItems(data);

        studentTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                editStudentButton.setDisable(false);
                deleteStudentButton.setDisable(false);
            } else {
                editStudentButton.setDisable(true);
                deleteStudentButton.setDisable(true);
            }
        });
    }

    // BUTTON CLICK HANDLERI
    public void onAddStudentClick(ActionEvent actionEvent) {
        openAddToTableView();
    }

    public void onEditStudentClick(ActionEvent actionEvent) {
        openEditInTableView();
    }
    public void onDeleteStudentClick(ActionEvent actionEvent) {
        openDeleteDialog();
    }

    private void openDeleteDialog(){
        ButtonType ok = new ButtonType("U redu", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Odustani", ButtonBar.ButtonData.CANCEL_CLOSE);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Potvrda brisanja");
        alert.setHeaderText("Označili ste studenta za brisanje");
        alert.setContentText("Želite li stvarno to učiniti?");

        alert.getButtonTypes().setAll(ok, cancel);

        Optional<ButtonType> result = alert.showAndWait();

        // AKO SE KLIKNE NA U REDU
        if(result.get() == ok){
            StudentModel sm = studentTable.getSelectionModel().getSelectedItem();
            sm.deleteStudent();
            initialize(null, null);
        }
    }

    private void openAddToTableView(){
        AddToTableOpener atto = new AddToTableOpener();

        atto.openAndInitAddToTableView(labelsText, viewName, captionInsert, insertSQL, this, null, true, getStudyGroups());
    }

    private void openEditInTableView(){
        StudentModel sm = studentTable.getSelectionModel().getSelectedItem();
        String[] selectedData = sm.getStudentRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsText, viewName, captionEdit, editSQL, this, selectedData, true, getStudyGroups());
    }

    private ObservableList<AddToTableModel> getStudyGroups(){
        String query = "SELECT idStudijskaGrupa, nazivStudijskaGrupa FROM studijska_grupa";
        String idColumnLabel = "idStudijskaGrupa";
        String nameColumnLabel = "nazivStudijskaGrupa";

        return AddToTableModel.listForeignKeyData(query, idColumnLabel, nameColumnLabel);
    }

    @Override
    public void callInitialize(){
        initialize(null,null);
    }

    @Override
    public void manyToManyInsert(String id, String querySQL){

    }
}
