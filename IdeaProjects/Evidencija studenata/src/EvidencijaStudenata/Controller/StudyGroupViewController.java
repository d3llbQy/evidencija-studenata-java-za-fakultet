package EvidencijaStudenata.Controller;

import EvidencijaStudenata.AddToTableOpener;
import EvidencijaStudenata.Model.StudyGroupModel;
import EvidencijaStudenata.Model.StudyGroupModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class StudyGroupViewController extends InitializeCall implements Initializable {
    @FXML
    TableView<StudyGroupModel> studyGroupTable;
    @FXML
    TableColumn<StudyGroupModel, String> idStudijskaGrupaCol;
    @FXML
    TableColumn<StudyGroupModel, String> nazivStudijskaGrupaCol;
    @FXML
    Button editStudGrpButton;
    @FXML
    Button deleteStudGrpButton;

    private String[] labelsText = {"ID:", "Naziv studijske grupe:"};
    private String viewName = "EvidencijaStudenata/View/AddToTableView.fxml";
    private String captionInsert = "Dodaj novu st. grupu";
    private String captionEdit = "Uredi st. grupu";
    private String insertSQL = "INSERT INTO studijska_grupa (idStudijskaGrupa, nazivStudijskaGrupa) VALUES(?,?)";
    private String editSQL = "UPDATE studijska_grupa SET nazivStudijskaGrupa = ? WHERE idStudijskaGrupa = ?";

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        editStudGrpButton.setDisable(true);
        deleteStudGrpButton.setDisable(true);

        ObservableList<StudyGroupModel> data = StudyGroupModel.listStudyGroup();
        idStudijskaGrupaCol.setCellValueFactory(new PropertyValueFactory<StudyGroupModel, String>("idStudijskaGrupa"));
        nazivStudijskaGrupaCol.setCellValueFactory(new PropertyValueFactory<StudyGroupModel, String>("nazivStudijskaGrupa"));
        studyGroupTable.setItems(data);

        studyGroupTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                editStudGrpButton.setDisable(false);
                deleteStudGrpButton.setDisable(false);
            } else {
                editStudGrpButton.setDisable(true);
                deleteStudGrpButton.setDisable(true);
            }
        });
    }

    // BUTTON CLICK HANDLERI
    public void onAddStudGrpClick(ActionEvent actionEvent) {
        openAddToTableView();
    }

    public void onEditStudGrpClick(ActionEvent actionEvent) {
        openEditInTableView();
    }

    public void onDeleteStudGrpClick(ActionEvent actionEvent) {
        openDeleteDialog();
    }

    private void openDeleteDialog(){
        ButtonType ok = new ButtonType("U redu", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Odustani", ButtonBar.ButtonData.CANCEL_CLOSE);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Potvrda brisanja");
        alert.setHeaderText("Označili ste st. grupu za brisanje");
        alert.setContentText("Želite li stvarno to učiniti?");

        alert.getButtonTypes().setAll(ok, cancel);

        Optional<ButtonType> result = alert.showAndWait();

        // AKO SE KLIKNE NA U REDU
        if(result.get() == ok){
            StudyGroupModel cm = studyGroupTable.getSelectionModel().getSelectedItem();
            cm.deleteStudyGroup();
            initialize(null, null);
        }
    }

    // IMPLEMENTACIJA IZ InitializeCall APSTRAKTNE KLASE
    @Override
    public void callInitialize(){
        initialize(null, null);
    }

    @Override
    public void manyToManyInsert(String id, String querySQL){

    }

    // POZIVACI METODA KLASE AddToTableViewController
    private void openAddToTableView(){
        AddToTableOpener atto = new AddToTableOpener();

        atto.openAndInitAddToTableView(labelsText, viewName, captionInsert, insertSQL, this, null, false, null);
    }

    private void openEditInTableView(){
        StudyGroupModel cm = studyGroupTable.getSelectionModel().getSelectedItem();
        String[] selectedData = cm.getStudyGroupRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsText, viewName, captionEdit, editSQL, this, selectedData, false, null);
    }
}
