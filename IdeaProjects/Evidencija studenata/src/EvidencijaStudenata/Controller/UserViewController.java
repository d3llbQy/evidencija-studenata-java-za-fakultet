package EvidencijaStudenata.Controller;

import EvidencijaStudenata.Model.UserModel;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class UserViewController implements Initializable{
@FXML
    TableView UserTableView;
@FXML
    TableColumn idUser;
@FXML
    TableColumn imeUser;
@FXML
    TableColumn prezimeUser;
@FXML
    TableColumn username;
@FXML
    TableColumn rolaUser;

    @Override
    public void initialize(URL url, ResourceBundle rb){
        ObservableList<UserModel> data = UserModel.listUser();

        idUser.setCellValueFactory(new PropertyValueFactory<UserModel, String>("idUser"));
        imeUser.setCellValueFactory(new PropertyValueFactory<UserModel, String>("imeUser"));
        prezimeUser.setCellValueFactory(new PropertyValueFactory<UserModel, String>("prezimeUser"));
        username.setCellValueFactory(new PropertyValueFactory<UserModel, String>("username"));
        rolaUser.setCellValueFactory(new PropertyValueFactory<UserModel, String>("rolaUser"));

        UserTableView.setItems(data);
    }
}

