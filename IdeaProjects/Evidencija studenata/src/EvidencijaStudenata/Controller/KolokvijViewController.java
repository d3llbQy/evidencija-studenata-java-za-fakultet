package EvidencijaStudenata.Controller;

import EvidencijaStudenata.AddToTableOpener;
import EvidencijaStudenata.Model.KolokvijModel;
import EvidencijaStudenata.Model.StudyGroupModel;
import EvidencijaStudenata.Model.UpisaniPredmetKolokvijModel;
import EvidencijaStudenata.Model.UserModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class KolokvijViewController extends InitializeCall{
    @FXML
    TableView<StudyGroupModel> studGrpOnKolokvijTable;
    @FXML
    TableColumn<StudyGroupModel, String> studGrpNameTableCol;
    @FXML
    TableView<UpisaniPredmetKolokvijModel> kolokvijTable;
    @FXML
    TableColumn<UpisaniPredmetKolokvijModel, String> studentNameAndSurnameTableCol;
    @FXML
    TableColumn<UpisaniPredmetKolokvijModel, String> pointsOnKolokvijNumberTableCol;
    @FXML
    Button addKolokvijPointsButton;

    private int kolokvijID;
    private String currID;
    private String[] labelsText = {"ID:", "Broj bodova:"};
    private String viewName = "EvidencijaStudenata/View/AddToTableView.fxml";
    private String captionEdit = "Uredi bodove na kolokviju";
    private String editSQL = "UPDATE kolokvij_upisani_predmet SET brojBodovaKolokvij = ? WHERE id_kolokvij_upisani_predmet = ?";

    private StudyGroupModel lastSelection;

    public void initKolokvijView(int kolokvijID, String currID){
        this.kolokvijID = kolokvijID;
        this.currID = currID;

        addKolokvijPointsButton.setDisable(true);

        initStudyGroupTable();
    }

    private void initStudyGroupTable(){
        ObservableList<StudyGroupModel> list = StudyGroupModel.listStudyGroupForSpecificCurr(currID);

        studGrpNameTableCol.setCellValueFactory(new PropertyValueFactory<StudyGroupModel, String>("nazivStudijskaGrupa"));
        studGrpOnKolokvijTable.setItems(list);

        studGrpOnKolokvijTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null) {
                lastSelection = newSelection;
                ObservableList<UpisaniPredmetKolokvijModel> listKol = UpisaniPredmetKolokvijModel.upisaniPredmetKolokvijList(newSelection.getIdStudijskaGrupa(), kolokvijID);

                studentNameAndSurnameTableCol.setCellValueFactory(new PropertyValueFactory<UpisaniPredmetKolokvijModel, String>("nameAndSurnameStudent"));
                pointsOnKolokvijNumberTableCol.setCellValueFactory(new PropertyValueFactory<UpisaniPredmetKolokvijModel, String>("brojBodovaKolokvij"));
                kolokvijTable.setItems(listKol);
            }

        });

        if(lastSelection != null){
            studGrpOnKolokvijTable.getSelectionModel().select(lastSelection);
        }

        kolokvijTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if(newSelection != null){
                addKolokvijPointsButton.setDisable(false);
            } else {
                addKolokvijPointsButton.setDisable(true);
            }
        });
    }

    // CLICK HANDLERI
    public void onAddKolokvijPointsButton(ActionEvent actionEvent) {
        openAddKolokvijPoints();
    }

    private void openAddKolokvijPoints(){
        UpisaniPredmetKolokvijModel upkm = kolokvijTable.getSelectionModel().getSelectedItem();
        String[] selectedData = upkm.getUPKRowByID();

        AddToTableOpener atto = new AddToTableOpener();
        atto.openAndInitAddToTableView(labelsText, viewName, captionEdit, editSQL, this, selectedData, false, null);
    }

    @Override
    public void callInitialize(){
        initKolokvijView(kolokvijID, currID);
    }

    @Override
    public void manyToManyInsert(String last_inserted_id, String querySQL){

    }

}
