package iss_prijava.Controller;

import iss_prijava.Model.Baza;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class UrediStudentaController{
    @FXML
    TextField Ime;
    @FXML
    TextField Prezime;
    @FXML
    TextField KorisnickoIme;
    @FXML
    TextField Lozinka;
    @FXML
    Label errorLabel;
    @FXML

    PregledStudentaController upc;
    int id_student;

    public void ulazUView(PregledStudentaController upc, int id_student){
        this.upc = upc;
        this.id_student = id_student;

    }

    public void dodajGumbKlik(ActionEvent event) {
        if(Ime.getText().equals("") || Prezime.getText().equals("") || KorisnickoIme.getText().equals("") || Lozinka.getText().equals("")){
            errorLabel.setVisible(true);
            errorLabel.setText("Ne mozete unijeti prazan naziv");
        } else{
            Baza baza = new Baza();
            PreparedStatement insert = baza.exec("UPDATE predmet SET ime = ?, prezime = ?, korisnicko_ime = ?, lozinka = ? WHERE id_user = ?");

            try{
                insert.setString(1, Ime.getText());
                insert.setString(2, Prezime.getText());
                insert.setString(3, KorisnickoIme.getText());
                insert.setString(2, Lozinka.getText());
                insert.setInt(2, id_student);

                insert.executeUpdate();
            } catch (SQLException e){
                System.out.println(e.getMessage());
            }
            errorLabel.getScene().getWindow().hide();
            upc.postaviTabeluStudenta();
        }
    }
}
