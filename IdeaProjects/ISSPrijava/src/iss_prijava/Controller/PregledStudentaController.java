package iss_prijava.Controller;

import iss_prijava.Model.Baza;
import iss_prijava.Model.StudentModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class PregledStudentaController implements Initializable{
    @FXML
    Label errorLabel;
    @FXML
    TableView<StudentModel> studentiTabela;
    @FXML
    TableColumn<StudentModel, String> brIndexKolona;
    @FXML
    TableColumn<StudentModel, String> imeStudent;
    @FXML
    TableColumn<StudentModel, String> prezimeStudent;
    @FXML
    TableColumn<StudentModel, String> korisnickoIme;
    @FXML
    TableColumn<StudentModel, String> lozinka;
    @Override
    public void initialize(URL url, ResourceBundle rb){
        postaviTabeluStudenta();
    }

    public void postaviTabeluStudenta(){
        brIndexKolona.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("id_user"));
        imeStudent.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("ime"));
        prezimeStudent.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("prezime"));
        korisnickoIme.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("korisnicko_ime"));
        lozinka.setCellValueFactory(new PropertyValueFactory<StudentModel, String>("lozinka"));

        studentiTabela.setItems(StudentModel.listaSvihStudenata());
    }

    public void dodajStudentaKlik(ActionEvent event) {
        dodajStudent();
    }

    private void dodajStudent(){
        try {
            Parent root;
            FXMLLoader loader = new FXMLLoader((getClass().getClassLoader().getResource("iss_prijava/View/DodajStudenta.fxml")));
            root = loader.load();
            DodajStudentaController dsc = loader.getController();
            dsc.ulazUView(this);
            Stage stage = new Stage();
            stage.setTitle("Dodaj novi student");
            stage.setScene(new Scene(root,400, 400));
            stage.show();
        } catch(IOException e){

        }
    }

    public void urediStudentaKlik(ActionEvent event) {

        if(studentiTabela.getSelectionModel().getSelectedItem() == null){
            errorLabel.setVisible(true);
            errorLabel.setText("Nije oznacen ni jedan redak u tablici");
        } else{
            errorLabel.setVisible(false);
            int id_student = studentiTabela.getSelectionModel().getSelectedItem().getId_user();

            try {
                Parent root;
                FXMLLoader loader = new FXMLLoader((getClass().getClassLoader().getResource("iss_prijava/View/UrediStudenta.fxml")));
                root = loader.load();
                UrediStudentaController usc = loader.getController();
                usc.ulazUView(this, studentiTabela.getSelectionModel().getSelectedItem().getIme(), studentiTabela.getSelectionModel().getSelectedItem().getId_user());
                Stage stage = new Stage();
                stage.setTitle("Uredi studenta");
                stage.setScene(new Scene(root,400, 400));
                stage.show();
            } catch(IOException e){

            }

        }


    }

    public void izbrisiStudentaKlik(ActionEvent event) {
        ButtonType okGumb = new ButtonType("Da", ButtonBar.ButtonData.OK_DONE);
        ButtonType neGumb = new ButtonType("Ne", ButtonBar.ButtonData.CANCEL_CLOSE);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Brisanje studenta");
        alert.setHeaderText("Potvrda brisanja");
        alert.setContentText("Zelite li stvarno izbrisati oznacenog studenta?");

        alert.getButtonTypes().setAll(okGumb , neGumb);

        Optional<ButtonType> result = alert.showAndWait();

        if(result.get() == okGumb){
            Baza baza = new Baza();
            PreparedStatement delete = baza.exec("DELETE FROM user WHERE id_user = ?");
            try{
                delete.setInt(1, studentiTabela.getSelectionModel().getSelectedItem().getId_user());
                delete.executeUpdate();
            } catch (SQLException e){
                System.out.println(e.getMessage());
            }
            postaviTabeluStudenta();

        }
    }
}
