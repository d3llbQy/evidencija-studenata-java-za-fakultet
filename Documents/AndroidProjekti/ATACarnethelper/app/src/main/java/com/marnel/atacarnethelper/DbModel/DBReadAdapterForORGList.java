package com.marnel.atacarnethelper.DbModel;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.marnel.atacarnethelper.R;

import java.util.ArrayList;

public class DBReadAdapterForList extends ArrayAdapter<DBAdapter.Organization> {
    ViewHolder holder;
    Context context;
    ArrayList<DBAdapter.Organization> items;
    private Activity activity;

    public DBReadAdapterForList(Context context, ArrayList<DBAdapter.Organization> items, Activity activity) {
        super(context, R.layout.list_item_row, items);

        this.context = context;
        this.items = items;
        this.activity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            rowView = inflater.inflate(R.layout.list_item_row, parent, false);

            holder = new ViewHolder();
            holder.id_row = (TextView) rowView.findViewById(R.id.id_row);
            holder.name_row = (TextView) rowView.findViewById(R.id.name_row);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.item = items.get(position);

        holder.id_row.setText(holder.item.id_organization);
        holder.name_row.setText(holder.item.name_organization);

        return rowView;
    }

    class ViewHolder {
        DBAdapter.Organization item;
        TextView id_row, name_row;
    }
}
