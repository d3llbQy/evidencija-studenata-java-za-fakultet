package com.jtvm.fpmozrecenzijeprofesora;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class MainActivity extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference databaseRef;
    EditText testET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = FirebaseDatabase.getInstance();
        databaseRef = database.getReference().child("Profesor");

        databaseRef.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        collectProfVals((Map<String, Object>)dataSnapshot.getValue());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );

    }

    private void collectProfVals(Map<String, Object> vals1){
        for(Map.Entry<String, Object> entry1 : vals1.entrySet() ){
            Map<String, Object> vals2 = (Map<String, Object>) entry1.getValue();
            for(Map.Entry<String, Object> entry2 : vals2.entrySet()){
                if(entry2.getKey().equals("profesor_data")){
                    Map<String, Object> vals3 = (Map<String, Object>) entry2.getValue();
                    for(Map.Entry<String, Object> entry3 : vals3.entrySet()){
                        if(entry3.getKey().equals("id_prof")){
                            String id_prof = entry3.getValue().toString();
                            int a = 3;
                        } else if (entry3.getKey().equals("Ime")){
                            String ime = entry3.getValue().toString();
                            int a = 3;
                        } else {
                            String prezime = entry3.getValue().toString();
                            int a = 3;
                        }
                    }
                }
            }

        }
    }

    protected void clickButtonClick(View v){

    }
}
